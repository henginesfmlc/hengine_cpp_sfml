#pragma once
#include "Basis.h"
class B2Shape : public Basis
{
protected:

	b2PolygonShape b2shape;
	b2BodyDef bdef;
	b2FixtureDef fixdef;
	b2Body* body;
	Shape shape;

public:
	
	B2Shape(v2f pos, v2f siz, bool isStatic = false);

	virtual void Update();
	virtual void Draw();
	virtual ~B2Shape(void);
};

