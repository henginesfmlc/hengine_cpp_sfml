#include "Grid.h"

CLR Grid::grid_clr;

sf::FloatRect Sector::rect_sector;

Grid::Grid(v2f pos, int style) : isAvailable(true)
{
	if(grid_style != style) grid_style = style;
	NextStyle();
	ConstructShape(shape, pos, size_grid, texture->Cubex);
	grid_clr = shape.getFillColor();
}

void Grid::NextStyle()
{
	if(grid_style > 3 || grid_style < 1) grid_style = 1;

	if(grid_style == 1) Style(sf::Color(20,140,20,75),		shape.getSize().x/10.f, sf::Color(0,120,0,45));			
	if(grid_style == 2) Style(sf::Color(220,220,100,100),	shape.getSize().x/10.f, sf::Color(180,100,180,180));  	
	if(grid_style == 3) Style(sf::Color(120,120,180,000),	shape.getSize().x/10.f, sf::Color(90,90,120,180));		
																													
	grid_clr = shape.getFillColor();																				
}																													
																													
void Grid::Draw()																									
{				
	wnd->draw(shape);																								
}																													
																													
void Grid::SetAvailable(bool isAvail) 																				
{ 																													
	isAvailable = isAvail; 																							
}																													

bool Grid::GetAvailable() const 
{ 
	return isAvailable; 
}

Grid::~Grid() 
{

}

void Grid::Style(sf::Color color, float outline, sf::Color outcolor)
{
	shape.setFillColor(color);
	shape.setOutlineThickness(outline);
	shape.setOutlineColor(outcolor);
}
