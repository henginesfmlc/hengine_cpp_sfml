#pragma once
#include "Button.h"

Button::Button(v2f pos, v2f siz, IMG& img, float text_size, const sf::String& str_text, bool perc_pos)
{
	is_Inside = is_Click = false;
	ConstructShape(shpButton, pos, siz, img, perc_pos);
	ConstructShape(glow, pos, siz, texture->Glow);
	ConstructText(text, shpButton.getPosition(), text_size, str_text, font_freshman);
	ConstructText(text, shpButton, str_text, font_freshman);
}

void Button::Update() 
{ 
	is_Inside ? text.setColor(sf::Color(180,180,180)) : text.setColor(sf::Color(255,255,255)); 
}

void Button::Draw()
{
	wnd->draw(shpButton);
	if(is_Inside) wnd->draw(glow);
	wnd->draw(text);
}

bool Button::Action()
{
	if(event.type == sf::Event::MouseButtonPressed || event.type == sf::Event::MouseMoved ) 
		shpButton.getGlobalBounds().contains(cur_p) ? is_Inside = true : is_Inside = false;

	if(event.type == sf::Event::MouseButtonPressed)
	{
		if(event.key.code == sf::Mouse::Left && is_Inside)
		{
			shpButton.setScale(v2f(.95f,.95f));
			is_Click = true;
		}
	}

	if(event.type == sf::Event::MouseButtonReleased)
	{
		if(event.key.code == sf::Mouse::Left && is_Click) 
		{
			is_Click = false; 
			shpButton.setScale(v2f(1,1));
			if(is_Inside) 
			{
				is_Inside = false;
				return true;
			}
		}
	}
	return false;
}

string Button::GetID() 
{ 
	return text.getString(); 
}

void Button::setPosition(const v2f& p)
{
	shpButton.setPosition(p);
	text.setPosition(p.x, p.y);
	glow.setPosition(p);
}

void Button::setTextString(const sf::String& str)
{
	text.setString(str);
}

void Button::setFillColor(sf::Color color)
{
	shpButton.setFillColor(color);
}

Button::~Button(void) { }
