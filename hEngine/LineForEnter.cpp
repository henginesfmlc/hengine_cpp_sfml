#include "LineForEnter.h"

LineForEnter::LineForEnter(v2f pos, v2f siz, IMG& img, bool isAct) : isAct(isAct)
{
	ConstructShape(shape, pos, siz, img, false);
	ConstructText(text, shape, "", font_freshman, sf::Color::Black);
	ConstructShape(shpInputLine, v2f(text.getPosition()), v2f(0.2f, siz.y*.75f), CLR::Black, false);
	numeration++;
}

LineForEnter::LineForEnter(v2f pos, v2f siz, CLR clr, float outl, CLR out_clr, bool isAct) : isAct(isAct)
{
	ConstructShape(shape, pos, siz, clr, false);
	shape.setOutlineThickness(outl*scr_1);
	shape.setOutlineColor(out_clr);
	ConstructText(text, shape, "", font_freshman, sf::Color::Black);
	ConstructShape(shpInputLine, v2f(text.getPosition()), v2f(0.2f, siz.y*.75f), CLR::Black, false);
	numeration++;
}

string& LineForEnter::GetData()
{
	return data;
}

bool& LineForEnter::IsAct()
{
	return isAct;
}

void LineForEnter::Update()
{
	if(isAct)
	{
		timer_transparent += time/2;
		shpInputLine.setFillColor(CLR(0,0,0,timer_transparent));
		if(timer_transparent > 254) timer_transparent = 0;
	}
}

void LineForEnter::Action()
{
	const v2f& p = shape.getPosition();
	const v2f& s = shape.getSize();
	const sf::FloatRect& rect_t = text.getGlobalBounds();

	if(event.type == sf::Event::MouseButtonPressed)
	{
		if(shape.getGlobalBounds().contains(cur_p)) isAct = true;
		else isAct = false;
	}

	if(isAct)
	{
		if(event.type == sf::Event::KeyPressed)
		{
			if(event.key.code == Key::LShift || event.key.code == Key::RShift) 
				isShift_Key_Pressed = true;

			if(shape.getSize().x >= rect_t.width*.9) // ������ �� ����� ������ �������� � ���� �����
			{
				if(event.key.code >= 0 && event.key.code < 26)	// ���� �������� �� A-Z
				{
					if(isShift_Key_Pressed) data += string(1, 'A' + uint(event.key.code)); 
					else data += string(1, 'a' + uint(event.key.code));
					text.setString(data);
				}
				else if(event.key.code > 25 && event.key.code < 36)	// ���� �������� �� 0-9
				{
					data += to_string(event.key.code - 26);
					text.setString(data);
				}
				else if(event.key.code == Key::Space) 
				{
					data.push_back('_');
					text.setString(data);
				}
				else if(event.key.code == Key::Return) 
					cout << " LineForEnter_" + to_string(numeration) + " = " + GetData() << endl; // ����� �������� ������ � �������
			}

			if(event.key.code == Key::BackSpace)
			{
				if(!data.empty()) data.erase(data.end()-1);
				text.setString(data);
			}
			CenteringText(text);
			shpInputLine.setPosition(p.x + (text.getGlobalBounds().width / 2.f), p.y);
		}

		if(event.type == sf::Event::KeyReleased)
		{
			if(event.key.code == Key::LShift || event.key.code == Key::RShift) 
				isShift_Key_Pressed = false;
		}
	}
}

void LineForEnter::Draw()
{
	wnd->draw(shape);
	if(isAct) wnd->draw(shpInputLine);
	wnd->draw(text);
}

LineForEnter::~LineForEnter(void)
{
}
