#pragma once
#include "Basis.h"

class BodyPart : public Basis
{
protected:

	string name;

public:

	Shape shape;

	BodyPart(v2f pos, v2f siz, const string name);

	virtual void Update();
	virtual void Draw();
	virtual string& GetName();

//	virtual void SetParam();

	virtual ~BodyPart();
};

class BodyPart_Cubex : public BodyPart
{
private:

public:

	BodyPart_Cubex(v2f pos, v2f siz, const string name);

	virtual void Update();
	virtual void Draw();
	virtual string& GetName();

	virtual ~BodyPart_Cubex();
};