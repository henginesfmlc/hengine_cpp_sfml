#include "BackGround.h"

BackGround::BackGround(void)
{
	vec_CubeFloat.push_back(make_shared<CubeFloat>(CubeFloat(v2f(rand()%50-50,rand()%50-50), v2f(15,15), 0.3f, 16)));
}

void BackGround::Update()
{
	for(auto& cube_float : vec_CubeFloat) cube_float->Update();
}

void BackGround::Draw()
{
	for(auto& cube_float : vec_CubeFloat) cube_float->Draw();
}

void BackGround::Action()
{
	for(auto& cube_float : vec_CubeFloat) cube_float->Action();
}


BackGround::~BackGround(void)
{

}
