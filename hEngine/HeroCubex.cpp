#include "Hero.h"

HeroCubex::HeroCubex(v2f pos, v2f siz, float ms, float hp, string name, int elements) : Unit_Body(pos, siz, ms, hp, name, elements), alpha(0.f), angle(0),  dalpha(0)
{
	m_ms_deft = ms;
	m_ms_bonus = 0.f;
	m_ms_curr = m_ms_deft + m_ms_bonus;

	m_hp_deft = 100;
	m_hp_bonus = 0;
	m_hp_curr = m_hp_deft + m_hp_bonus;

	m_accelerate = 0.f;

	for(int i = 0; i < this->m_elements; i++) 
	{
		m_vec_Part.push_back(make_shared<BodyPart_Cubex>(BodyPart_Cubex(GetNormalizedPosition(pos, 3, i*(360/this->m_elements)), siz, "part_"+to_string(i))));
	}
	Recolor();
}

void HeroCubex::Update()
{
	m_accelerate = GetDistance(shpCursor.getPosition(), m_center) / ( scr_H / 4);
	if(m_accelerate > .05f) 
		MoveToAngle(m_center, m_ms_curr * m_accelerate, GetAngle(m_center, cur_p));

	if(dalpha > 50) 
	{ 
		dalpha -= time; 
		alpha += (m_ms_curr / 3.f * (dalpha / 300)) * time; 
	}
	else
	{
		if(dalpha < -50) 
		{ 
			dalpha += time;
			alpha -= (m_ms_curr / 3.f * (-dalpha / 300) ) * time; 
			if(alpha < 0.f) 
			{
				alpha = 0.f; 
				dalpha = 0.f;
			}
		}
		else dalpha = 0.f;
	}

	if(isMouseL) angle -= 0.1*time;
	if(isMouseR) angle += 0.1*time;

	
	int ibody = 0;
	for(auto& part : m_vec_Part)
	{
		const float SIZ = part->shape.getSize().x + alpha;
		part->shape.setPosition(GetNormalizedPosition(m_center, SIZ, angle + ibody * (360 / m_elements))); 
		++ibody;
	}

	static float timer_create_spec_effect = 0;
	timer_create_spec_effect += time;

	if(timer_create_spec_effect > 40) 
	{
		timer_create_spec_effect = 0;
		for(auto& body_part : m_vec_Part)
			vec_SpecEffect.push_back(make_shared<SpecEffect_CubexSpark>(SpecEffect_CubexSpark(body_part->shape, 500, 0.05)));
	}

	for(auto& spec_effect = vec_SpecEffect.begin(); spec_effect != vec_SpecEffect.end();)
	{
		auto& it = *(*spec_effect);
		if(it.IsEnd()) spec_effect = vec_SpecEffect.erase(spec_effect);
		else
		{
			it.Update();
			spec_effect++;
		}
	}
}

void HeroCubex::Action() 
{ 
	if(event.type == sf::Event::MouseButtonPressed)
	{
		if(event.key.code == sf::Mouse::Left) isMouseL = true;
		if(event.key.code == sf::Mouse::Right) isMouseR = true;
	}
	if(event.type == sf::Event::MouseButtonReleased)
	{
		if(event.key.code == sf::Mouse::Left) isMouseL = false;
		if(event.key.code == sf::Mouse::Right) isMouseR = false;
	}

	if(event.type == sf::Event::MouseWheelMoved)
	{
		dalpha += event.mouseWheel.delta*300;
		if(dalpha > 1500) dalpha = 1500;
		else if (dalpha < -1500) dalpha = -1500;
	}
}

void HeroCubex::Draw()
{
	for(auto& spec_effect : vec_SpecEffect) spec_effect->Draw();
	Unit_Body::Draw();
}

void HeroCubex::Recolor()
{
	int i = 0;
	for(auto& part : m_vec_Part)
	{
		int c = 55 + (i*(200 / m_elements));
		part->shape.setOutlineThickness(-part->shape.getSize().x/10.f);
		part->shape.setFillColor(sf::Color(c*i,c*55,c*(i+55)));
		part->shape.setOutlineColor(sf::Color(c*0.75,c*0.75,c*0.75));
		++i;
	}
}

HeroCubex::~HeroCubex() {}
