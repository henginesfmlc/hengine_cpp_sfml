#include "TimerPereodic.h"

TimerPereodic::TimerPereodic(int n_order, float time_order, int extra_order, float extra_time, bool infinite) 
	: time_order(0), 
	order_current(0), 
	MAX_ORDERS_DEFAULT_(n_order), 
	MAX_TIME_DEFAULT_(time_order),
	_EXTRA_ORDERS(extra_order), 
	_EXTRA_TIME(extra_time),
	infinite(infinite)
{

}

bool TimerPereodic::TickEnd()
{
	time_order += B::time;
	
	if(time_order > MAX_TIME_DEFAULT_ + _EXTRA_TIME)
	{
		time_order = 0;
		order_current++;
		if(order_current >= MAX_ORDERS_DEFAULT_ + _EXTRA_ORDERS) 
			if(infinite) order_current = 0;
		return true;
	}
	return false;
}

void TimerPereodic::ExtraOrder(int extra)
{
	_EXTRA_ORDERS = extra;
}

void TimerPereodic::ExtraTime(float extra)
{
	_EXTRA_TIME = extra;
}

TimerPereodic::~TimerPereodic(){}