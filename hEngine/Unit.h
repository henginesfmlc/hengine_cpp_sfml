#pragma once
#include "BodyPart.h"
#include "SpecEffect.h"
class Unit : public B
{
protected:

	float m_hp_curr;
	int m_hp_deft;
	int m_hp_bonus;


	float m_ms_deft;
	float m_ms_bonus;
	float m_ms_curr;

	string m_name;

public:

	Shape m_shape;

	virtual void Update();
	virtual void Draw();
	virtual void Animation();
	virtual string GetName();
	Unit(float ms, float hp, string name);
	virtual ~Unit(void);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Unit_Body �� ��������� Unit, �� �� ������� ��������. /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Unit_Body : public B
{
protected:

	float m_hp_curr;
	int m_hp_deft;
	int m_hp_bonus;

	float m_ms_deft;
	float m_ms_bonus;
	float m_ms_curr;

	string m_name;

	int m_elements;
	float m_accelerate;

public:

	vector<shared_ptr<BodyPart>> m_vec_Part;
	v2f m_center;

	virtual void Update();
	virtual void Draw();
	virtual void Animation();
	virtual void PushPart(v2f p, v2f s, string name, float rotate = 0, bool is_perc_pos = true);
	virtual void PushPartCircle(v2f p, v2f s, const string name, int num, float rotation = 0, bool is_perc_pos = true);

	virtual string GetName();
	virtual int GetElements() const;
	Unit_Body(v2f pos, v2f siz, float hp, float ms, string name, int elements);
	virtual ~Unit_Body();
};


