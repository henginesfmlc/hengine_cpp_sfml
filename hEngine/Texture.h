#pragma once
#include "Define.h"

class Texture
{
private: 
	
	inline void Load(IMG& texture, const string& file);

public: 

	// User Interface
	IMG Cursor;
	IMG Button;
	IMG ButtonQuadIco;
	IMG Glow;

	// Game Object
	IMG DarkEye;
	IMG PortalEffectFire, PortalEffectLife, PortalEffectShadow, PortalEffectFrost;
	IMG Box[2];
	IMG Wall_128x128_3x3_x4_01;
	IMG WallSkyLine, WallSky, WallSkyHalfLine, WallSkyClear;
	IMG Cubex;
	IMG Lighting_16;

	// Unit
	IMG Emitter;

	Texture();
	void setSmoth(bool isSmooth);
	~Texture(void);
};