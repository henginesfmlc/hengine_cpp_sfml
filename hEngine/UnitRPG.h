#pragma once
#include "Unit.h"

class UnitRPG : public Unit
{
protected:

	int m_dmg_bonus;					// �������� ���� � ����
	int m_dmg_deft;						// ��������� ���� � ����
	int m_dmg_curr;						// ������� ����

	float m_atck_dist_deft;				// ��������� ����� � % ������
	float m_atck_dist_bonus;			//
	float m_atck_dist_curr;				//

	int m_mp_deft;						// ����
	int m_mp_bonus;						// ������������ ����� ����
	int m_mp_curr;						// ������� ����� ����

	bool m_is_life;						// ���
	bool m_is_attack_delay_sucefull;	// ����� ������ �������
	bool m_is_atck_delay;				// ���� ��� ����������� �� �������� ����� �� �����
	bool m_is_melee;					// ������� ���

	v2f m_spawn_point;					// ����� ������
	bool m_is_agro;						// ��������� � ����� �� �����
	bool m_is_agressive;				// ��������� �� ������� ������
	bool m_is_escape;					// ������������ �� �� ����� ������

	TimerPereodic timerP_AtckColdowner;	// ���������� ����� ����-�������
	TimerPereodic timerP_Animation;		// ������ ��������
	TimerPereodic timerP_AttackDelay;	// ������ ��
	TimerPereodic timerP_Corpse;		// ������ ���������� �����

	enum STATE 
	{ 
		ATTACK_DEAL,					// ���������
		CAST_ABILITY_TARGET,			// ����� � �������� ����������� � ����������� �����-����
		CAST_ABILITY_CHANNEL,			// ��������� � ��������� ������ ����������
		FOLLOW_TO_TARGET,				// ������� �� �����
		STAND,							// ����� �� �����
		DIES,							// �������
		RETURN_TO_SPAWN					// ������������ �� ����� ������
	} m_state; // ���������

	static float DIST_AGRESSIVE_ON;		// ��������� ��������� ������� ��������
	static float DIST_AGRESSIVE_OFF;	// ��������� �������� ������� � ������
	static float DIST_LOST_TARGET;		// ��������� ������ �������� � ����

public:

	virtual void EscapeToSpawn();									// ������������ �� �����
	virtual void SetAgro(bool agro);								// ��������
	virtual bool GetAgressive() const;								// �����������
	virtual bool GetAgro() const;									// ��������� � ����� �� �����
	virtual bool GetIsEscape() const;								// ������������ �� �� ����� ������
	virtual v2f GetSpawnPoint() const;								// ����� ������
	virtual void Draw();

	virtual void Attack(UnitRPG* attacked_unit);					// �������� �������� �����
	virtual void AttackDealDamage(UnitRPG* damaged_unit);			// ������� ����
	virtual int GetAttackDamage() const;							// ���������� ����� ���������� ����������
	virtual float GetAttackDist() const;							// ��������� �����
	virtual bool GetIsAttackDelay() const;							// ���� ��� ����������� �� �������� ����� �� �����
	virtual void AttackDelay();										// ���������� �����

	virtual void RestoreHp(int restore);							// ������������ �������� �� restore ��.
	virtual void RestoreMP(int restore);							// ������������ ���� �� restore ��.

	virtual void Revive(v2f position, uint HP, uint MP);			// ���������� � ��������� �������� � ���� � ��������� �����
	virtual void Revive(v2f position);								// ���������� � ������ ��������� � �����

	virtual void SetHpMax(uint hp);									// �������� ������������ �������� ��������
	virtual void SetMpMax(uint mp);									// �������� ������������ �������� ����
	virtual void SetHp(uint hp);									// �������� ������� �������� ��������
	virtual void SetMp(uint mp);									// �������� ������� �������� ��������
	virtual void SetSpeed(float speed);								// �������� �������� �����������
	virtual void SetState(const uint state);						// �������� ���������
	virtual void Corpse();											// ��������� ����

	virtual int GetHpMax() const;									// ������������ ����� ��������
	virtual int GetMpMax() const;									// ������������ ����� ����
	virtual int GetHp() const;										// �������� �������
	virtual int GetMp() const;										// ���� �������
	virtual enum STATE GetState() const;							// ��������� ��������
	virtual float GetMoveSpeed() const;								// �������� �����������
	virtual bool GetIsLife() const;									// �����
	virtual void AnimationPlay(string id_animation);				// �������������� ��������

	virtual void SetParamDeft(float hp, float mp, float ms, float dmg, float atck_dist);
	virtual void SetParamBonus(float hp, float mp, float ms, float dmg, float atck_dist);
	virtual void SetParamCurr(float hp, float mp, float ms, float dmg, float atck_dist);

	UnitRPG(v2f pos, v2f siz, float hp, float mp, float ms, float dmg, float atck_dist, bool isMelee, string name);
};
