#pragma once
#include "Menu_MapEditor.h"
#include "Level.h"
#include "MapEditor.h"
#include "BackGround.h"

class MainMenu : public B
{
private:

	BackGround BG;
	vector<shared_ptr<Button>> vec_Button;
	vector<shared_ptr<LineForEnter>> vec_LineForEnter;
	auto_ptr<Menu_MapEditor> menu_MapEditor;

	enum MSTATE { MAIN, SETTINGS, FAST_GAME, LOAD_GAME, COMPANY, MAP_EDITOR, EXIT } StateMainMenu;

public:

	
	virtual void Draw();
	virtual void Action();
	virtual void Update();
	virtual void ClearButton();
	virtual void OpenSettings();
	virtual void OpenFastGame();
	virtual void OpenLoadGame();
	virtual void OpenCompany();
	virtual void OpenMapEditor();
	virtual void OpenExit();
	virtual void OpenMainMenu();

	MainMenu(void);
	~MainMenu(void);
};

