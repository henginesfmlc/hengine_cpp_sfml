#include "Missle.h"

Missle::Missle(v2f pos, v2f siz, float speed, int dmg, float angle, sf::Color color) : 
	timerP_CreateSpecEffect(1, 40, 0, false),
	speed(speed),
	damage(dmg)
{

	vec_Body.push_back(make_shared<BodyPart_Cubex>(BodyPart_Cubex(pos, siz,"missle")));
	vec_Body[0]->shape.setRotation(angle);
}

void Missle::SpecEffectUpdate()
{
	if(timerP_CreateSpecEffect.TickEnd())
	{
		Shape& body_shape = vec_Body[0]->shape;
		vec_SpecEffect.push_back(make_shared<SpecEffect_CubexSpark>(SpecEffect_CubexSpark(body_shape.getPosition(), body_shape.getSize()*0.4f, sf::Color(200,80,80,100), body_shape.getRotation(), 500, 0)));
	}

	for(auto& it = vec_SpecEffect.begin(); it != vec_SpecEffect.end();)
	{
		auto& spec_effect = *(*it);
		if(spec_effect.IsEnd()) it = vec_SpecEffect.erase(it);
		else
		{
			spec_effect.Update();
			it++;
		}
	}
}

void Missle::Update()
{
	SpecEffectUpdate();
	for(auto& part : vec_Body) 
		MoveToAngle(part->shape, speed, part->shape.getRotation());
}

void Missle::Draw()
{
	for(auto& spec_effect : vec_SpecEffect) spec_effect->Draw();
	for(auto& body_part : vec_Body) body_part->Draw();
}

float Missle::GetDamage() const { return damage; }
Shape& Missle::GetShape() { return vec_Body.back()->shape; }

Missle::~Missle(void) {}

//================================================================================================================================================================================================================
//================================================================================================================================================================================================================
//================================================================================================================================================================================================================

MissleTarget::MissleTarget(v2f pos, v2f siz, float speed, int dmg, float angle, UnitRPG& target_unit, IMG& img) 
	: Missle(pos, siz, speed, dmg, angle, CLR::White), m_target_unit(&target_unit)
{
	vec_Body.push_back(make_shared<BodyPart_Cubex>(BodyPart_Cubex(pos, siz,"missle")));
	vec_Body[0]->shape.setRotation(angle);
	for(auto& part : vec_Body) part->shape.setTexture(&img);
}

void MissleTarget::Update()
{
	Missle::SpecEffectUpdate();
	MoveToAngle(GetShape(), speed, GetAngle(m_target_unit->m_shape, GetShape()));
}

MissleTarget::~MissleTarget(void) {}