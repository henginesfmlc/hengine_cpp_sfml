#include "RightClickMenu.h"

RightClickMenu::RightClickMenu(v2f pos) : TIMER_CREATE(500), timer_create(0), count_button(0)
{

}


void RightClickMenu::Update() 
{ 
	if(timer_create > TIMER_CREATE)
	{
		for(auto button : vec_Button) 
			button->Update(); 
	}
	else RightClickMenu::Create();
}


void RightClickMenu::Push(v2f siz, IMG& img, string name)
{
	count_button++;
	vec_Button.push_back(make_shared<Button>(Button(v2f(cur_p.x, cur_p.y + (count_button*siz.y*scr_1)), siz, img, 0.25f, name, false)));
}

void RightClickMenu::Clear()
{
	for(auto& it = vec_Button.begin(); it != vec_Button.end();)
	{
		it = vec_Button.erase(it);
		it++;
	}
	vec_Button.clear();
	count_button = 0;
}

string RightClickMenu::Action()
{
	for(auto button : vec_Button)
	{
		if(button->Action())
		{
			if(timer_create < 0)
			{
				return button->GetID();
			}
		}
	}
	return "";
}

void RightClickMenu::Draw()
{
	for(auto button : vec_Button) button->Draw();
}


void RightClickMenu::Create()
{
	timer_create += time;
	if(timer_create < TIMER_CREATE)
	{
		for(auto& button : vec_Button)
		{
			button->shpButton.setScale(timer_create/TIMER_CREATE,1);
			button->glow.setScale(timer_create/TIMER_CREATE,1);
		}
	}
	else
	{
		for(auto& button : vec_Button)
		{
			button->shpButton.setScale(1,1);
			button->glow.setScale(1,1);
		}
	}
}


RightClickMenu::~RightClickMenu(void)
{
}
