#pragma once
#include "CubeFloat.h"

class BackGround : public B
{
private:

	vector<shared_ptr<SpecEffect>> vec_SpecEffect;
	vector<shared_ptr<CubeFloat>> vec_CubeFloat;

public:

	virtual void Update();
	virtual void Draw();
	virtual void Action();
	BackGround(void);
	~BackGround(void);
};



