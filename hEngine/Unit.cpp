#include "Unit.h"

Unit::Unit(float ms, float hp, string name) : m_name(name)
{
	m_hp_deft = hp;
	m_hp_bonus = 0;
	m_hp_curr = m_hp_bonus + m_hp_deft;

	m_ms_deft = ms;
	m_ms_bonus = 0.00f;
	m_ms_curr = m_ms_deft + m_ms_bonus;
}

void Unit::Update()
{

}

void Unit::Draw()
{
	wnd->draw(m_shape);
}

void Unit::Animation()
{
	// isAnimation
}

string Unit::GetName()
{
	return m_name;
}

Unit::~Unit(void)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Unit_Body �� ��������� Unit, �� �� ������� ��������. /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Unit_Body::Unit_Body(v2f pos, v2f siz, float hp, float ms, string name, int elements) 
	: 
	m_elements(elements), 
	m_name(name), 
	m_center(pos*scr_1)
{
	m_hp_deft = hp;
	m_hp_bonus = 0;
	m_hp_curr = m_hp_deft + m_hp_bonus;

	m_ms_deft = ms;
	m_ms_bonus = 0;
	m_ms_curr = m_ms_deft + m_ms_bonus;

	m_accelerate = 0;
}

int Unit_Body::GetElements() const
{
	return m_elements;
}

void Unit_Body::Update()
{
	for(auto part : m_vec_Part) part->Update();
}

void Unit_Body::PushPart(v2f pos, v2f siz, string name, float rotation, bool is_perc_pos)
{
	if(is_perc_pos) pos*=scr_1;
	m_vec_Part.push_back(make_shared<BodyPart_Cubex>(BodyPart_Cubex(pos,siz, name)));
	m_vec_Part.back()->shape.setRotation(rotation);
}

void Unit_Body::PushPartCircle(v2f pos, v2f siz, string name, int num, float rotation, bool is_perc_pos)
{
	m_vec_Part.clear();
	if(m_elements + num > 0 && m_elements + num < 10)
	{
		m_elements+=num;
		for(int i = 0; i < m_elements; i++) 
		{
			m_vec_Part.push_back(make_shared<BodyPart_Cubex>(
				BodyPart_Cubex(GetNormalizedPosition(pos, 3, i*(360/m_elements)),siz,"part_"+to_string(i))));
			m_vec_Part.back()->shape.setRotation(rotation);
		}
	}
}

void Unit_Body::Draw()
{
	for(auto part : m_vec_Part) part->Draw();
}

void Unit_Body::Animation()
{
	// isAnimation
}

string Unit_Body::GetName()
{
	return m_name;
}

Unit_Body::~Unit_Body(void)
{
}


