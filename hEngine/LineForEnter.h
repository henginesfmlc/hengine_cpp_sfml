#pragma once
#include "Button.h"

class LineForEnter : public B
{
protected:

	bool isAct;					// ���� ������ �������
	string data;				// �� ��� �� �����
	sf::Text text;				// ����� ��� �����������
	float timer_transparent;	// ������ ������� �������
	Shape shpInputLine;			// �������� ������� ��� ����� ������

public:
	
	Shape shape;
	
	LineForEnter(v2f pos, v2f siz, IMG& img, bool isActive = true);
	LineForEnter(v2f pos, v2f siz, CLR clr, float outline, CLR olclr, bool isActive = true);

	virtual bool& IsAct();
	virtual void Update();
	virtual void Action();
	virtual void Draw();
	virtual string& GetData();
	virtual ~LineForEnter(void);
};
