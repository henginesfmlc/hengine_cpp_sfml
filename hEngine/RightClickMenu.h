#pragma once
#include "button.h"
class RightClickMenu : public B
{
protected:

	vector<shared_ptr<Button>> vec_Button;
	float timer_create;			// ����� �������� ���������
	const float TIMER_CREATE;	// ��������� ���������� ������� timer_create / TIMER_CREATE = 1.0f 
	int count_button;			// ������� ������ � ������� ����

public:

	RightClickMenu(v2f pos);

	virtual void Update();
	virtual string Action();
	virtual void Draw();

	// ������� ��������� ������
	virtual void Create();
	// ������� ������ �� ������
	virtual void Clear();
	// �������� ������ � ����
	virtual void Push(v2f siz, IMG& img, string name);

	virtual ~RightClickMenu(void);
};
