#pragma once
#include "Basis.h"

class Button : public Basis
{
protected:

	bool is_Inside;
	bool is_Click;
	
public:

	Shape glow,shpButton;
	sf::Text text;

	Button::Button(v2f pos, v2f siz, IMG& img, float text_size, const sf::String& str_text, bool perc_pos = true);

	virtual void Update();
	virtual void Draw();
	virtual bool Action();
	virtual string GetID();

	virtual void setPosition(const v2f& p);
	void setTextString(const sf::String& str);
	virtual void setFillColor(sf::Color color);
	virtual ~Button(void);
};