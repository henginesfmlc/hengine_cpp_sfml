#include "Menu_MapEditor.h"


Menu_MapEditor::Menu_MapEditor(void)
{
	vec_Button.push_back(make_shared<Button>(Button(v2f(-44,-44), v2f( 8,4),  texture->ButtonQuadIco, 2, "MapSize")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-36,-44), v2f( 8,4),  texture->ButtonQuadIco, 2, "Gravity")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-40), v2f(16,4),  texture->ButtonQuadIco, 2, "PlayerControll")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-36), v2f(16,4),  texture->ButtonQuadIco, 2, "Weightlessness")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-32), v2f(16,4),  texture->ButtonQuadIco, 2, "ConditionVictory")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-28), v2f(16,4),  texture->ButtonQuadIco, 2, "Name")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-24), v2f(16,4),  texture->ButtonQuadIco, 2, "HeroAttribute")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-20), v2f(16,4),  texture->ButtonQuadIco, 2, "Description")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-16), v2f(16,4),  texture->ButtonQuadIco, 2, "BackGround")));

	vec_Button.push_back(make_shared<Button>(Button(v2f(-44,44), v2f(8,4),  texture->ButtonQuadIco, 2, "Back")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-36,44), v2f(8,4),  texture->ButtonQuadIco, 2, "Create")));
}

void Menu_MapEditor::Update()
{
	for(auto& button : vec_Button) button->Update();
	for(auto& line_fe : vec_LineEnter) line_fe->Update();
}

void Menu_MapEditor::Draw()
{
	for(auto& line_fe : vec_LineEnter) line_fe->Draw();
	for(auto& button : vec_Button) button->Draw();
}

void Menu_MapEditor::Action()
{
	for(auto& button : vec_Button) 
	{
		if(button->Action())
		{
			if(button->GetID() == "MapSize") 
			{ 
				vec_LineEnter.push_back(make_shared<LineForEnter>(LineForEnter(v2f(cur_p.x, cur_p.y), v2f(6,2.5), CLR(200,200,200), 0.3, CLR(80,80,80), true)));
				vec_LineEnter.push_back(make_shared<LineForEnter>(LineForEnter(v2f(cur_p.x, cur_p.y+(scr_1*2.5)), v2f(20,2.5), CLR(200,200,200), 0.3, CLR(80,80,80), false)));
				vec_Button.clear();
				break;
			}

			if(button->GetID() == "Gravity") 
			{ 

			}
			if(button->GetID() == "PlayerControll") 
			{ 

			}
			if(button->GetID() == "Weightlessness") 
			{ 

			}
			if(button->GetID() == "ConditionVictory") 
			{ 

			}
			if(button->GetID() == "Name") 
			{ 

			}
			if(button->GetID() == "HeroAttribute") 
			{ 

			}
			if(button->GetID() == "Description") 
			{ 

			}
			if(button->GetID() == "BackGround") 
			{ 

			}
			if(button->GetID() == "Back") 
			{ 

			}
			if(button->GetID() == "Create") 
			{ 

			}
		}
	}

	for(auto& line : vec_LineEnter) 
	{
		line->Action();
		
	}
}


Menu_MapEditor::~Menu_MapEditor(void)
{
}
