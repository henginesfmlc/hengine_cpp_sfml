#pragma once
#include "Hero.h"
#include "UnitRPG.h"
#include "Box.h"
#include "Grid.h"
#include "B2Shape.h"

class Level : public B
{
private:

	vector<shared_ptr<HeroCubex>> vec_Hero;
	vector<shared_ptr<UnitRPG>> vec_UnitRPG;
	vector<shared_ptr<MissleTarget>> vec_Missle;
	vector<shared_ptr<Box>> vec_Box;
	vector<shared_ptr<Sector>> vec_Sector;
	vector<shared_ptr<B2Shape>> vec_B2Shape;

	TimerPereodic TimerP_Regeneration;

public:

	// ����� ����� � ������

	virtual bool LoadOnFile();
	virtual void Restart();	  
	virtual void Update();	  
	virtual void Draw();	  
	virtual void Action();	  

	Level(v2i size_map, string name);
	virtual ~Level(void);
};

