#include "B2Shape.h"
#include <iostream>

B2Shape::B2Shape(v2f pos, v2f siz, bool isStatic) 
{
	b2shape.SetAsBox(siz.x * scr_1 / 2 , siz.y * scr_1 / 2 );
	bdef.position.Set(pos.x , pos.y );
	if(isStatic) bdef.type = b2_staticBody;
	else bdef.type = b2_dynamicBody;

	body = world->CreateBody(&bdef);

	body->SetFixedRotation(false);
	body->CreateFixture(&b2shape, 1);

	fixdef.density = 1.f;		// ���������
	fixdef.friction = 1.f;		// ���� ������
	fixdef.restitution = 1.f;	// ���������
	fixdef.shape = &b2shape;	// ��� ������
	//fixdef.

	// body->ApplyLinearImpulse(b2v(0, -5), body->GetWorldCenter(), false);
	// body->ApplyForce(b2v(40,40), body->GetWorldCenter(), false);
	// body->SetAngularVelocity(30);

	ConstructShape(shape, pos, siz, texture->Box[1], false);
}

void B2Shape::Update()
{
	shape.setPosition(body->GetPosition().x, body->GetPosition().y);
	shape.setRotation(body->GetAngle() * DEG);
}

void B2Shape::Draw()
{
	wnd->draw(shape);
}

B2Shape::~B2Shape(void)
{
}
