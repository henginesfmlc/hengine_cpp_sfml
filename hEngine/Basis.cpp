#include "Basis.h"

sf::RenderWindow*		Basis::wnd;
sf::Clock				Basis::clock;
sf::Event				Basis::event;
sf::View				Basis::cam;
sf::Font*				Basis::font_freshman;
b2World*				Basis::world;
b2v						Basis::gravity;
float					Basis::time;
float					Basis::time_in_pause;
float					Basis::time_enemy;
int						Basis::scr_W;
int						Basis::scr_H;
float					Basis::scr_1;
float					Basis::scr_1w;
v2f						Basis::cam_p;
v2f						Basis::cur_p;
Texture*				Basis::texture;
Audio*					Basis::audio;
enum Basis::GAME_STATE	Basis::GameState;
bool					Basis::isPauseGame;
bool					Basis::isSound_Music_On;
bool					Basis::isSound_Effects_On;
bool					Basis::isVideo_Smoth_On;
bool					Basis::isVideo_VertSync_On;
bool					Basis::isShift_Key_Pressed;
uint					Basis::sound_Vol_Music;
uint					Basis::sound_Vol_Effects;
Shape					Basis::shpCursor;
v2f						Basis::center_map;
v2f						Basis::size_grid;
int						Basis::grid_style;
int						Basis::numeration;

bool Basis::WordInString(string& str, const string word)
{
	int n_word = 0;	// ���������� ���� � �����
	for(auto& w: word) n_word++;

	cout << n_word << endl;

	string search;

	for(auto& c : str)
	{
		if(c == word[0]) 
		{
			for(int i = 0; i < n_word; i++) search.push_back(c);
			if(search == word) return true;
			else search.clear();
		}
	}
	return false;
}

v2f Basis::v2fInString(string& str)
{
	v2f v;
	string search = "v2f/(";

	for(auto& c : str)
	{
		if(c == 'v') 
		{
			for(int i = 0; i < 4; i++) search.push_back(c);
			if(search == "v2f(")
			{
				// ������� ������� �� ������� � ���������� x �������
				string x = 0;
				if(c != ',') x.push_back(c);
				else v.x = std::stof(x);
				return v;	
			}
			else search.clear();
		}
	}
	return v2f(0,0);
}

sf::FloatRect Basis::GetVisible()
{
	return sf::FloatRect(float(cam_p.x - (scr_W/2)), float(cam_p.y - (scr_H/2)),  float(scr_W),  float(scr_H));
}

void Basis::ReStyleCursor(const string name)
{
	wnd->setMouseCursorVisible(false);
	if(name == "default") ConstructShape(shpCursor, cur_p, v2f(1.25,1.25), texture->Cubex);
	else if(name == "target") ConstructShape(shpCursor, cur_p, v2f(2.5,2.5), texture->Cursor);
	else
	{
		shpCursor = Shape();
		wnd->setMouseCursorVisible(true);
	}
}

void Basis::Update()
{
	time = float(clock.getElapsedTime().asMicroseconds() / 1000.f), clock.restart();
	time_enemy = time;
	if(!isPauseGame) time_in_pause = time;
	else time_in_pause = 0;
	cur_p = wnd->mapPixelToCoords(sf::Mouse::getPosition(*wnd));
	shpCursor.setPosition(cur_p);
	cam_p = cam.getCenter();
	world->Step(1.f/60,8,3);
}

v2f Basis::GetNormalizedPosition(const v2f& pos, float dist, float angle)
{
	const float& A = angle * PI / 180.f;
	return pos + v2f(cosf(A),sinf(A))*dist;
}

v2f Basis::GetNormalizedPosition(Shape& shape, float dist, float angle)
{
	const float& A = angle;
	return shape.getPosition() + v2f(cosf(A),sinf(A))*dist;
}

void Basis::MoveToAngle(v2f& point, float speed, float angle)
{
	const float& A = angle;
	const float& S = speed * scr_1 * time;
	point += v2f(cosf(A),sinf(A))*S;
}

void Basis::MoveToAngle(Shape &shape, float speed, float angle)
{
	const float& A = angle;
	const float& S = speed * scr_1 * time;
	shape.move(v2f(cosf(A),sinf(A))*S); 
}

bool Basis::GetBoundRect(sf::FloatRect& r1, sf::FloatRect& r2)
{
	return r1.intersects(r2);
}

bool Basis::GetBoundRect(Shape& shape1, Shape& shape2)
{
	const sf::FloatRect& r1 = shape1.getGlobalBounds();
	const sf::FloatRect& r2 = shape2.getGlobalBounds();
	return r1.intersects(r2);
}

float Basis::GetDistance(const v2f& p1, const v2f& p2)
{
	return sqrt(pow(p1.x - p2.x,2) + pow(p1.y - p2.y,2));
}

bool Basis::GetCollisionCircle(Shape& c1, Shape& c2)
{
	const float r1 = c1.getSize().x / 2.f;
	const float r2 = c2.getSize().x / 2.f;
	const v2f& p1 = c1.getPosition();
	const v2f& p2 = c2.getPosition();
	return GetDistance(p1, p2) > float(r1 + r2) ? false : true;
}

float Basis::GetDistance(Shape& s1, Shape& s2)
{
	const float r1 = s1.getSize().x / 2.f;
	const float r2 = s2.getSize().x / 2.f;
	const v2f& p1 = s1.getPosition();	const v2f& p2 = s2.getPosition();
	return GetDistance(p1, p2);
}

float Basis::GetDistance(Shape& s1, const  v2f& pos)
{
	const float r1 = s1.getSize().x / 2.f;
	const v2f& p1 = s1.getPosition();
	return GetDistance(p1, pos) > r1;
}

void Basis::ConstructShape(Shape& shp, v2f pos, v2f siz, IMG& png, bool perc_pos)
{
	shp.setSize(siz*scr_1);
	shp.setOrigin(siz*scr_1/2.f);
	if(perc_pos) shp.setPosition(pos*scr_1);
	else shp.setPosition(pos);
	shp.setTexture(&png);
}

void Basis::ConstructShape(Shape& shp, v2f pos, v2f siz, bool perc_pos)
{
	shp.setSize(siz*scr_1);
	shp.setOrigin(siz*scr_1/2.f);
	if(perc_pos) shp.setPosition(pos*scr_1);
	else shp.setPosition(pos);
}

void Basis::ConstructShape(Shape& shp, v2f pos, v2f siz, CLR clr, bool perc_pos)
{
	shp.setSize(siz*scr_1);
	shp.setOrigin(siz*scr_1/2.f);
	shp.setFillColor(clr);
	if(perc_pos) shp.setPosition(pos*scr_1);
	else shp.setPosition(pos);
}

void Basis::ConstructText(sf::Text& text, Shape& shape, sf::String str, sf::Font* font, CLR clr)
{
	const v2f& p = shape.getPosition();
	const v2f& s = shape.getSize();

	text.setFont(*font);
	text.setStyle(sf::Text::Bold);
	text.setCharacterSize(s.y*.65f);
	text.setColor(clr);
	text.setString(str);
	text.setPosition(p);
	CenteringText(text);
}

void Basis::ConstructText(sf::Text& text, v2f pos, float siz, sf::String str, sf::Font* font, CLR clr, bool perc_pos)
{
	text.setFont(*font);
	text.setStyle(sf::Text::Bold);
	text.setCharacterSize(uint(siz*scr_1));
	text.setColor(clr);
	text.setString(str);
	
	if(perc_pos) text.setPosition(pos*scr_1);
	else text.setPosition(pos);

	CenteringText(text);
}

void Basis::CenteringText(sf::Text& text)
{
	const v2f&  o = v2f(text.getGlobalBounds().width / 2.f, text.getGlobalBounds().height / 1.5f);
	text.setOrigin(o);
}

float Basis::GetAngle(const v2f& p1, const v2f& p2)
{
	return atan2f(p2.y - p1.y, p2.x - p1.x);
}

float Basis::GetAngle(Shape& shp, const v2f& p2)
{
	return atan2f(p2.y - shp.getPosition().y, p2.x - shp.getPosition().x);
}

float Basis::GetAngle(Shape& shp1, Shape& shp2)
{
	return atan2f(shp2.getPosition().y - shp1.getPosition().y, shp2.getPosition().x - shp1.getPosition().x);
}

Basis::Basis(bool init)
{
	if(init)
	{
		numeration = 0;
		grid_style = 1;
		gravity = b2v(0,40);
		size_grid = v2f(2.5f, 2.5f);
		isPauseGame = false;
		isShift_Key_Pressed = false;
		GameState = GAME_STATE::MENU;	
		scr_W = GetSystemMetrics(0); 	
		scr_H = GetSystemMetrics(1);	
		scr_1 = scr_H / 100.f;			
		scr_1w = scr_W / 100.f;			
		time_in_pause = 0;				
		time = 0;						
		center_map = v2f(0,0);		
		texture = new Texture;
		audio = new Audio;
		texture->setSmoth(true);
		font_freshman = new sf::Font;
		font_freshman->loadFromFile("Resources/Font/freshman.ttf");

		cam.reset(sf::FloatRect(0, 0, float(scr_W), float(scr_H)));
		cam.setCenter(0,0);
		cam_p = cam.getCenter();
		cur_p = v2f(0,0);
		wnd = new sf::RenderWindow(sf::VideoMode(scr_W,scr_H), "SFML Game Template", sf::Style::Fullscreen, sf::ContextSettings(0,0,24));
		ReStyleCursor("default");
		wnd->setView(cam);
		wnd->setActive(false);
		srand(::time(0));

		world = new b2World(gravity);
		clock.restart();
	}
}

Basis::Basis(void){}
Basis::~Basis(void){}

/* �������������� ��������
if(!is_file_settings_ok)
{
	ofstream settings("Settings/Settings.prm");
	settings << "50";	// Effect VOL
	settings << "\n1";	// Effect ON
	settings << "\n50";	// Music VOL
	settings << "\n1";	// Music ON
	settings << "\n1";	// Smotsh ON
	settings << "\n0";	// VertSync ON
	settings << "\n1.0";// ModifierSpeed
	settings << "\n1";	// EffectStar ON
	settings << "\n0";	// Language 0 EN 1 RU
	settings.close();

	isVideo_Smoth_On = true;
	isVideo_VertSync_On = false;
	isEffectStar = true;
	isSound_Effects_On = true;
	isSound_Music_On = true;
	sound_Vol_Effects = 50;
	sound_Vol_Music = 50;
	modifier_speed = 1.0000000;
	language = LANGUAGE::EN;
}

if(isVideo_Smoth_On) 
	texture->setSmoth(true);

if(!isSound_Effects_On) 
	sound->setVolEffects(0);
else sound->setVolEffects(sound_Vol_Effects);

if(!isSound_Music_On) 
	sound->setVolMusic(0);
else sound->setVolMusic(sound_Vol_Music);
*/