#pragma once
#include "MainMenu.h"

class Game
{
private:
	
	vector<shared_ptr<Level>> vec_Level;
	vector<shared_ptr<MapEditor>> vec_MapEditor;
	vector<shared_ptr<MainMenu>> vec_MainMenu;
	sf::Thread* thread_Update;

public:

	void ThreadUpdate();

	Game(void);
	~Game(void);
};

