#pragma once
#include "Box.h"

class Generator : public B
{
private:

	int battery;
	bool isLoading;
	sf::Text text;

public:

	Shape shape;

	Generator(v2f pos, v2f siz, CLR clr) : battery(0)
	{
		ConstructShape(shape, pos, siz);
		ConstructText(text, shape.getPosition(), 2, to_string(battery) + "\%", font_freshman, CLR::White);
	}

	virtual void Loading()
	{
		if(!isLoading)
		{
			battery += time / 1000;
			if(battery >= 100) { isLoading = true; battery = 100; }
		}
	}



	virtual void Update()
	{

	}

	virtual void Draw()
	{
		wnd->draw(shape);
		wnd->draw(text);
	}

	virtual ~Generator() {}
};