#pragma once
#include "Define.h"
#include "Texture.h"
#include "Audio.h"

class Basis
{
public:

	Basis(void);		
	Basis(bool init);	
	~Basis(void);
	static void Update();
	// �����s ������� ������ ������� sf::RectangleShape. 
	// siz = siz*scr_1 perc_pos = false ��� pos. ( �� ��������� pos * scr_1 )
	static void ConstructShape(Shape& shp, v2f pos, v2f siz, IMG& png, bool perc_pos = true);
	static void ConstructShape(Shape& shp, v2f pos, v2f siz, bool perc_pos = true);
	static void ConstructShape(Shape& shp, v2f pos, v2f siz, CLR clr, bool perc_pos = true);

	// ����� ������� ������ ������� sf::Text.
	static void ConstructText(sf::Text& text, v2f pos, float siz, sf::String str, sf::Font* font, CLR clr = CLR::Cyan, bool perc_pos = false);
	static void ConstructText(sf::Text& text, Shape& shape, sf::String str, sf::Font* font, CLR clr = CLR::Cyan);
	static void CenteringText(sf::Text& text);

	// ����� ����� �������
	static void ReStyleCursor(const string name);

public:

	static sf::RenderWindow* wnd;
	static sf::Font* font_freshman;
	static Texture* texture;
	static Audio* audio;
	static b2World* world;
	static b2v gravity;

	static sf::Clock clock;							
	static sf::Event event;							
	static sf::View cam;

	static Shape shpCursor;	


	static float time;								
	static float time_in_pause;						
	static float time_enemy;						
	static int scr_W;								
	static int scr_H;								
	static float scr_1;								
	static float scr_1w;							
	static v2f cam_p;								
	static v2f cur_p;			

	static enum GAME_STATE { MENU, GAME, MAP_EDITOR } GameState;	
	static bool isPauseGame;			
	static v2f center_map;
	static int grid_style;
	static int numeration;

	static bool isSound_Music_On;					
	static bool isSound_Effects_On;					
	static bool isVideo_Smoth_On;					
	static bool isVideo_VertSync_On;		
	static bool isShift_Key_Pressed;

	static uint sound_Vol_Music;					
	static uint sound_Vol_Effects;			
	static v2f size_grid;

public:

	// �������� 4 ���� ������ ����. ������������ ��� �������� �� ����������� �������, ��� �����������.
	static sf::FloatRect GetVisible();
	// �������� ������������ ���� �����
	static bool  GetCollisionCircle(Shape& s1, Shape& s2);
	// ������ �������� ������������ ���� ��������������� 
	static bool GetBoundRect(Shape& shape1, Shape& shape2);
	static bool GetBoundRect(sf::FloatRect& r1, sf::FloatRect& r2);

	// �������� ��������� �� ����� p1 �� p2
	static float GetDistance(const v2f& p1, const v2f& p2);
	// �������� ��������� �� ������ ������� �� ����� pos
	static float GetDistance(Shape& shape, const v2f& pos);
	// �������� ��������� �� ������ �� ������ �������� sf::RectangleShape
	static float GetDistance(Shape& s1, Shape& s2);
	// �������� ����� ��� ��������� ������� � ����� �� ����� pos � ������� �� �����������
	static v2f GetNormalizedPosition(const v2f& pos, float dist, float angle);	
	// �������� ����� ��� ��������� ������� � ����� �� ������� shape � ������� �� �����������
	static v2f GetNormalizedPosition(Shape& shape, float dist, float angle);
	// �������� ���� �� ����������� ����� ����� ������� p1 � p2
	static float GetAngle(const v2f& p1, const v2f& p2);
	// �������� ���� �� ����������� ����� ������� ������� shape � ������ pos
	static float GetAngle(Shape& shp, const v2f& pos);
	// �������� ���� �� ����������� ����� �������� �������� shape
	static float GetAngle(Shape& shp1, Shape& shp2);
	// ������� shape �� �����������, ���������� ������ ��������
	static void MoveToAngle(Shape& shape, float speed, float angle);
	/* ������� ����� �� �����������, ���������� ������ ��������.
	������������ ��� ����������� ��� �� ���������� �������� BodyPart */
	static void MoveToAngle(v2f& pos, float speed, float angle);	

public:

	static bool WordInString(string& str, const string word);
	static v2f v2fInString(string& str);
};