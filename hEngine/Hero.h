#pragma once
#include "Missle.h"

class Hero : public Unit
{
protected:
	
	bool isMouseL, isMouseR;

public:

	Hero(v2f pos, float ms, float hp, string name);
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Recolor() = 0;
	virtual void Action() = 0;
	virtual void PushBody(int num) = 0;
	virtual ~Hero();
};

class HeroCubex : public Unit_Body
{
protected:

	bool isMouseL, isMouseR;

	float alpha;
	float dalpha;
	float angle;
	
public:

	vector<shared_ptr<SpecEffect>> vec_SpecEffect;
	HeroCubex(v2f pos, v2f siz, float ms, float hp, string name, int elements);

	virtual void Update();
	virtual void Action();
	virtual void Draw();
	virtual inline void Recolor();
	virtual ~HeroCubex();
};

class Arnold : public Unit
{
private:
	
	bool m_Is_Move_Left;		// ����������� �������� �����
	bool m_Is_Move_Right;		// ����������� �������� ������
	bool m_Is_Move_Down;		// ����������� �������� ����
	bool m_Is_Move_Up;			// ����������� �������� �����

	bool m_Is_Attack_Can;		// ����� ����

	float m_Energy_Point;		// ������� ��� ��������� ����
	int m_Energy_Point_Max;		// ������� ������������ �����
	int m_Energy_Cost;			// ��������� �����
	float m_Energy_Regeneration;// �������� ����������� �������

	float m_Factor_Assitude;	// ������ ��������� 0.0 - 1.0

	virtual float GetMoveDirection();
	virtual void UpdateCamera();
	virtual void UpdateBar();
	virtual void MobAgro();
	virtual void HideNeutralBar(bool hide);
	virtual void PickTargetUnit();
	virtual void EnergyRegeneration();

	Shape UI_Bar_HP_Arnold;				// ��� ��������
	Shape UI_Bar_Energy_Arnold;			// ��� �������
	Shape UI_Ico_Arnold;				// ������ ��������
	Shape UI_Indicator_HP_Arnold;		// ��������� ��������
	Shape UI_Indicator_MP_Arnold;		// ��������� ����
	Shape UI_Indicator_Energy_Arnold;	// ��������� �������
	Shape UI_Indicator_Coldown_Arnold;	// ��������� ����������� �����

	Shape UI_Bar_HP_Neutral;			// ��� �������� �����
	Shape UI_Ico_Neutral;				// ������ �����
	Shape UI_Indicator_HP_Neutral;		// ��������� �������� �����
	Shape UI_Indicator_MP_Neutral;		// ��������� ���� �����

	Unit* m_Picked_Unit;				// ��������� ����
	Unit* m_Target_Unit;				// ��������� ����


public:

	void Update();
	void Control();
	
	
	Arnold(v2f position, v2f size, string name_ID);

};