#include "Box.h"

Box::Box(v2f pos, v2f siz, IMG& img, CLR clr, float out_line, CLR out_clr)
{
	ConstructShape(shape, pos, siz, img, false);
	SetStyle(clr, out_line, out_clr);
}

Box::Box(v2f pos, v2f siz, CLR clr, float out_line, CLR out_clr)
{
	ConstructShape(shape, pos, siz, false);
	SetStyle(clr, out_line, out_clr);
}

void Box::SetStyle(CLR clr, float out_line, CLR out_clr)
{
	shape.setFillColor(clr);
	shape.setOutlineThickness(out_line);
	shape.setOutlineColor(out_clr);
}

void Box::SetStyle(CLR clr, float out_line, CLR out_clr, IMG& img) 
{
	SetStyle(clr, out_line, out_clr);
	shape.setTexture(&img);
}

void Box::SetStyle(CLR clr, IMG& img)
{
	shape.setFillColor(clr);
	shape.setTexture(&img);
}
void Box::Draw() 
{ 
	wnd->draw(shape); 
}

void Box::Update() { }
void Box::Action() { }

Box::~Box() { }

//================================================================================================================================================================
//================================================================================================================================================================
//================================================================================================================================================================

BoxResize::BoxResize(v2f pos, v2f siz, IMG& img) : Box(pos, siz, img), alpha(0)
{
	ConstructShape(shape, pos, siz, img, false);
	size = shape.getSize();
}

BoxResize::BoxResize(v2f pos, v2f siz) : Box(pos, siz), alpha(0)
{
	ConstructShape(shape, pos, siz, false);
	size = shape.getSize();
}

void BoxResize::Update()
{
	alpha += time/1000.f;
	shape.setSize(size+size*abs(cos(alpha)));
	shape.setOrigin(shape.getSize()/2.f);
}

void BoxResize::Draw()
{ 
	wnd->draw(shape); 
}

void BoxResize::Action() 
{

}

BoxResize::~BoxResize() 
{

}
