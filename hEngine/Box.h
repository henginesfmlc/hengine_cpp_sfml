#pragma once
#include "SpecEffect.h"

class Box : public B
{
protected:

	vector<shared_ptr<SpecEffect>> vec_SpecEffect;

public:

	Shape shape;

	Box(v2f pos, v2f siz, IMG& img, CLR clr = CLR::White, float out_line = 0.f, CLR out_clr = CLR::White);
	Box(v2f pos, v2f siz, CLR clr = CLR::White, float out_line = 0.f, CLR out_clr = CLR::White);

	virtual void SetStyle(CLR color, float out_line, CLR out_color);
	virtual void SetStyle(CLR color, float out_line, CLR out_color, IMG& img);
	virtual void SetStyle(CLR color, IMG& img);

	virtual void Update();
	virtual void Draw();
	virtual void Action();

	virtual ~Box();
};

class BoxResize : public Box
{
private:

	float alpha;
	v2f size;

public:

	BoxResize(v2f pos, v2f siz, IMG& img);
	BoxResize(v2f pos, v2f siz);

	virtual void Update();
	virtual void Draw();
	virtual void Action();

	virtual ~BoxResize();
};