#include "BodyPart.h"

BodyPart_Cubex::BodyPart_Cubex(v2f pos, v2f siz, const string name) : BodyPart(pos, siz, name)
{
	ConstructShape(shape, pos, siz, false);
}

void BodyPart_Cubex::Update()
{

}

void BodyPart_Cubex::Draw()
{
	wnd->draw(shape);
}

string& BodyPart_Cubex::GetName() { return name; }

BodyPart_Cubex::~BodyPart_Cubex(){}