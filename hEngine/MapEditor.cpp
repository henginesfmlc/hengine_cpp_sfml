#include "MapEditor.h"

MapEditor::MapEditor(v2i size_map, string name) : map_name(name), state(Default)
{
	vec_Sector.clear();

	v2i num_Grid_In_Sector(scr_W/(size_grid.x*scr_1)/2, scr_H/(size_grid.y*scr_1)/2);	// ���������� ����� � � ������� �������� 1/4 �� ���������� ������
	v2i num_Sectors(size_map.x/num_Grid_In_Sector.x, size_map.y/num_Grid_In_Sector.y);	// ���������� �������� �����

	// ��������� ����� ����� � ������ ���� ������
	center_map = v2f(num_Grid_In_Sector.x * num_Sectors.x * (size_grid.x * scr_1), num_Grid_In_Sector.y * num_Sectors.y * (size_grid.y * scr_1));
	cam.setCenter(center_map);
	wnd->setView(cam);

	for(int i = 0; i < num_Sectors.x; i++)
		for(int j = 0; j < num_Sectors.y; j++)
		{
			v2f pos = v2f(i*num_Grid_In_Sector.x*size_grid.x, j*num_Grid_In_Sector.y*size_grid.x);	// ����� ������ ��������� �������
			vec_Sector.push_back(make_shared<Sector>(Sector(num_Grid_In_Sector, pos)));				// ��������� ������ ��������
		}

	ConstructShape(brush_shape_ghost_object, cur_p, size_grid * 0.49f, texture->ButtonQuadIco, false);
	brush_shape_ghost_object.setFillColor(CLR(255,255,255,100));
}

void MapEditor::CheckSizeBrush(string matrix__2x2__5x5_)
{
	if(matrix__2x2__5x5_ == "1x1") ConstructShape(brush_shape_ghost_object, cur_p, size_grid * 1.49f, texture->ButtonQuadIco, false);
	if(matrix__2x2__5x5_ == "2x2") ConstructShape(brush_shape_ghost_object, cur_p, size_grid * 2.49f, texture->ButtonQuadIco, false);
	if(matrix__2x2__5x5_ == "3x3") ConstructShape(brush_shape_ghost_object, cur_p, size_grid * 3.49f, texture->ButtonQuadIco, false);
	if(matrix__2x2__5x5_ == "4x4") ConstructShape(brush_shape_ghost_object, cur_p, size_grid * 4.49f, texture->ButtonQuadIco, false);
	if(matrix__2x2__5x5_ == "5x5") ConstructShape(brush_shape_ghost_object, cur_p, size_grid * 5.49f, texture->ButtonQuadIco, false);
}

void MapEditor::Draw()
{
	BG.Draw();
	switch (state)
	{
		case MapEditor::ObjectInstall:
			break;
		case MapEditor::InRightCM:
			break;
		case MapEditor::InButtonCursor:
			break;
		case MapEditor::Default:
			break;
	}

	for(auto& sector : vec_Sector)				sector->Draw();
	for(auto& unit : vec_Unit)					unit->Draw();
	for(auto& spec_effect : vec_SpecEffect)		spec_effect->Draw();

	wnd->draw(brush_shape_ghost_object);

	if(state == STATE::ObjectInstall)
	{
		wnd->draw(brush_shape_ghost_object);
	}

	if(state == STATE::InRightCM)
	{
		for(auto& rc_menu : vec_RCMenu)
			rc_menu->Draw();
	}
}

void MapEditor::Update()
{
	BG.Update();

	if(GetDistance(center_map, cur_p) > scr_1 * 48)
	{
		MoveToAngle(center_map, 0.03, GetAngle(center_map, cur_p));
		cam.setCenter(center_map);
		wnd->setView(cam);
	}

	for(auto& sector : vec_Sector)
	{
		if(sector->GetIsActive()) // ����� ��������� ������� �� ���������� � ������
		{
			for(auto& grid : sector->vec_Grid)
			{
				if(GetBoundRect(grid->shape, brush_shape_ghost_object))
				{
					
					grid->shape.setFillColor(sf::Color(0,150,255,200));
					grid->shape.setOutlineColor(sf::Color(0,150,255,255));
					grid->shape.setScale(v2f(0.9, 0.9));
					if(grid->shape.getGlobalBounds().contains(cur_p))
						brush_shape_ghost_object.setPosition(grid->shape.getPosition());
				}
				else
				{
					grid->shape.setFillColor(grid->grid_clr);
					grid->shape.setScale(v2f(1,1));
				}
			}
		}
	}

	switch (state)
	{
		// ��������� ����� ����� ������ ��� ���������� ������ � ���������� ��� ��������� �� �����
	case MapEditor::ObjectInstall:

		break; // ����� ��������� ObjectInstall	MapEditor::Update()

		// ��������� ����� ����� ����� ��� �� ������� ����� �������� ��� ���������
	case MapEditor::InRightCM:		

		for(auto& rc_menu: vec_RCMenu) rc_menu->Update();

		break; // ����� ��������� InRightCM MapEditor::Update()

		// ��������� ����� ������ ��������� �� ������ ��� ������ ������ ����������������� ����������
	case MapEditor::InButtonCursor:
		
		break; // ����� ��������� InButtonCursor MapEditor::Update()

		// ������������ ���������
	case MapEditor::Default:		

		break; // ����� ��������� Default MapEditor::Update()
	}

	// �������� ����������� �� ��������� �������
	for(auto& it = vec_SpecEffect.begin(); it != vec_SpecEffect.end();)
	{
		auto& spec_effect = *(*it);

		if(spec_effect.IsEnd()) 
		{
			it = vec_SpecEffect.erase(it); 
		}
		else
		{
			spec_effect.Update();
			it++;
		}
	}
}

void MapEditor::Action()
{
	BG.Action();
	if(event.type == sf::Event::KeyPressed) 
	{
		if(event.key.code == Key::O)
		{
			grid_style++;

			for(auto& sector : vec_Sector)
				for(auto& grid : sector->vec_Grid) 
					grid->NextStyle();

			if(grid_style >= 3) grid_style = 0;
		}
		if(event.key.code == Key::X) cam.rotate(45);
		if(event.key.code == Key::F1) CheckSizeBrush("1x1");
		if(event.key.code == Key::F2) CheckSizeBrush("2x2");
		if(event.key.code == Key::F3) CheckSizeBrush("3x3");
		if(event.key.code == Key::F4) CheckSizeBrush("4x4");
		if(event.key.code == Key::F5) CheckSizeBrush("5x5");
	}

	switch (state)
	{
	case MapEditor::ObjectInstall:

		if(event.type == sf::Event::MouseButtonPressed)
		{
			if(event.key.code == sf::Mouse::Left)
			{
				// paste object in greed position
			}
		}

		break;

	case MapEditor::InRightCM:

		for(auto& rc_menu: vec_RCMenu) 
		{
			if(rc_menu->Action() == "Copy")
			{

			}
		}

		if(event.type == sf::Event::MouseButtonPressed)
		{
			if(event.key.code == sf::Mouse::Right)
			{

			}
			if(event.key.code == sf::Mouse::Left)
			{

			}
		}

		break;

	case MapEditor::InButtonCursor: break;

	case MapEditor::Default:

		if(event.type == sf::Event::MouseButtonPressed)
			if(event.key.code == sf::Mouse::Right)
			{
				if(vec_RCMenu.empty())
				{
					state = MapEditor::InRightCM;
					vec_RCMenu.push_back(make_shared<RightClickMenu>(RightClickMenu(cur_p)));
					vec_RCMenu.back()->Push(v2f(10,3), texture->ButtonQuadIco, "Copy");
					vec_RCMenu.back()->Push(v2f(10,3), texture->ButtonQuadIco, "Rotate");
					vec_RCMenu.back()->Push(v2f(10,3), texture->ButtonQuadIco, "Rename");
					vec_RCMenu.back()->Push(v2f(10,3), texture->ButtonQuadIco, "Size");
					vec_RCMenu.back()->Push(v2f(10,3), texture->ButtonQuadIco, "Color");
					vec_RCMenu.back()->Push(v2f(10,3), texture->ButtonQuadIco, "Close");
				}
			}
		break;
			default: break;
	}
}

Basis MapEditor::SelectObject()
{
	return Unit(0.03, 100, "PickedObject");
}

bool MapEditor::LoadMap(string file_name)
{
	return true;
}

void MapEditor::NewMap(v2f size_map, v2f greed_size, string name)
{
	map_name = name;
	vec_Sector.clear();

	v2i num_Grid_In_Sector(scr_W/(size_grid.x*scr_1), scr_H/(size_grid.y*scr_1));
	v2i num_Sectors(size_map.x/num_Grid_In_Sector.x, size_map.y/num_Grid_In_Sector.y);

	for(int i = 0; i < num_Sectors.x; i++)
	{
		for(int j = 0; j < num_Sectors.y; j++)
		{
			v2f pos = v2f(i*num_Grid_In_Sector.x*size_grid.x, j*num_Grid_In_Sector.y*size_grid.x);
			vec_Sector.push_back(make_shared<Sector>(Sector(num_Grid_In_Sector, pos)));
		}
	}

	// ��������� ����� ����� � ������ ���� ������
	center_map = v2f(num_Sectors.x * num_Grid_In_Sector.x * size_grid.x * scr_1, num_Sectors.y * num_Grid_In_Sector.y * size_grid.y * scr_1) / 2.f;
	cam.setCenter(center_map);
	wnd->setView(cam);
}

string MapEditor::PickedObject()
{
	//return obj.getName();
	return "";
}

void MapEditor::SaveMap(string file_name)
{
	ofstream fout(file_name+".hmap");
    fout << "������ � ������� � �++";
    fout.close();
}

MapEditor::~MapEditor(void)
{
}
