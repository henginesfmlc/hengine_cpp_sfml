#pragma once
#include "basis.h"

// ������ ������������� ������������, 
// ������� ��������

class TimerPereodic
{
protected:

	bool infinite;
	float time_order;
	
	int _EXTRA_ORDERS;
	const int MAX_ORDERS_DEFAULT_;

	float _EXTRA_TIME;
	const float MAX_TIME_DEFAULT_;
	
public:

	int	order_current;

	TimerPereodic(int n_order, float time_order, int extra_order = 0, float extra_time = 0, bool infinite = true);

	virtual void ExtraOrder(int extra);
	virtual void ExtraTime(float extra);
	virtual bool TickEnd();
	virtual ~TimerPereodic();
};