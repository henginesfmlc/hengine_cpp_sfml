#include "UnitRPG.h"

float UnitRPG :: DIST_AGRESSIVE_ON;
float UnitRPG :: DIST_AGRESSIVE_OFF;
float UnitRPG :: DIST_LOST_TARGET;

void UnitRPG :: SetParamDeft(float hp, float mp, float ms, float dmg, float atck_dist)
{
	m_hp_deft		 = hp;
	m_mp_deft		 = mp;
	m_ms_deft		 = ms;
	m_atck_dist_deft = atck_dist*scr_1;
}
void UnitRPG :: SetParamBonus(float hp, float mp, float ms, float dmg, float atck_dist)
{
	m_hp_bonus		  = hp;
	m_mp_bonus		  = mp;
	m_ms_bonus		  = ms;
	m_atck_dist_bonus = atck_dist*scr_1;
}
void UnitRPG :: SetParamCurr(float hp, float mp, float ms, float dmg, float atck_dist)
{
	m_hp_curr		 = hp;
	m_mp_curr		 = mp;
	m_ms_curr		 = ms;
	m_atck_dist_curr = atck_dist;
}

void UnitRPG :: Draw() { wnd->draw(m_shape); }

UnitRPG :: UnitRPG(v2f pos, v2f siz, float hp, float mp, float ms, float dmg, float atck_dist, bool isMelee, string name) 
	: Unit(hp, ms, name),
	timerP_Corpse(20, 1000, 0, 0, false),
	timerP_AttackDelay(1, 300),
	timerP_AtckColdowner(1,1800),
	timerP_Animation(3, 100), 
	m_is_melee(isMelee)
{
	if(m_name == "RangeUnit" || m_name == "MeleeUnit")
	{
		SetParamDeft(hp, mp, ms, dmg, atck_dist);
		SetParamBonus(0,0,0,0,0);
		m_is_escape = false;
		ConstructShape(m_shape, pos, siz, texture->Wall_128x128_3x3_x4_01, false);
		m_shape.setTextureRect(sf::IntRect(rand()%2*384,rand()%2*384,384,384));
	}

	SetParamCurr((m_hp_deft + m_hp_bonus), (m_mp_bonus + m_mp_deft), (m_ms_deft + m_ms_bonus), (m_dmg_deft + m_dmg_bonus), (m_atck_dist_deft + m_atck_dist_bonus));
	m_is_agro = false;
	m_is_agressive = true;
	m_spawn_point = m_shape.getPosition();

	m_is_atck_delay		= false;
	m_is_life			= true;
	m_state				= STAND;
}

void UnitRPG :: EscapeToSpawn()
{
	if(GetDistance(m_spawn_point, m_shape.getPosition()) > B::scr_1*50)
	{
		MoveToAngle(m_shape, m_ms_curr, GetAngle(m_shape.getPosition(), m_spawn_point));
		m_hp_curr = m_hp_deft + m_hp_bonus;
		m_mp_curr = m_mp_deft + m_mp_bonus;
	}
	else
	{
		m_state = UnitRPG::STAND;
		m_is_agro = false;
	}
}

v2f UnitRPG :: GetSpawnPoint() const { return m_spawn_point; }

bool UnitRPG :: GetAgro() const { return m_is_agro; }

bool UnitRPG :: GetAgressive() const { return m_is_agressive; }

void UnitRPG :: SetAgro(bool agro) { m_is_agro = agro; }

bool UnitRPG :: GetIsEscape() const { return m_is_escape; }

void UnitRPG :: AttackDealDamage(UnitRPG* damaged_unit)
{
	 damaged_unit->m_hp_curr -= m_dmg_curr;	// ������� ����
	 
	 if(damaged_unit->m_hp_curr <= 0)
	 {
	 	damaged_unit->m_hp_curr = 0; // ������� ����
	 	damaged_unit->m_is_life = false;
	 	damaged_unit->m_is_atck_delay = false; // ��������� ����� �� �����
	 	damaged_unit->m_shape.setFillColor(CLR::White);
	 }
}

void UnitRPG :: RestoreHp(int restore_HEAL)
{
	m_hp_curr += restore_HEAL;
	if(m_hp_curr > m_hp_deft + m_hp_bonus)
		m_hp_curr = m_hp_deft + m_hp_bonus;
}

void UnitRPG :: RestoreMP(int restore_MANA)
{
	m_mp_curr += restore_MANA;
	if(m_mp_curr > m_mp_deft + m_mp_bonus)
		m_mp_curr = m_mp_deft + m_mp_bonus;
}

void UnitRPG :: Revive(v2f pos, uint HP_PERCENTAGE, uint MP_PERCENTAGE)
{
	if(!m_is_life)
	{
		m_shape.setPosition(pos);
		m_hp_curr = (m_hp_bonus + m_hp_deft) / 100 * HP_PERCENTAGE;
		m_mp_curr = (m_mp_bonus + m_mp_deft) / 100 * MP_PERCENTAGE;
		m_is_life = true;
	}
}

void UnitRPG :: Revive(v2f pos)
{
	if(!m_is_life)
	{
		m_shape.setPosition(pos);
		m_hp_curr = m_hp_bonus + m_hp_deft;
		m_mp_curr = m_mp_bonus + m_mp_deft;
		m_is_life = true;
	}
}


void UnitRPG :: AttackDelay() {
	m_is_atck_delay = true;
}

void UnitRPG :: Attack(UnitRPG* attacked_unit)
{
	if(timerP_AtckColdowner.TickEnd())	// ���� ��������� ��������������
	{
		m_shape.setFillColor(CLR::Red);
	
		if(timerP_AttackDelay.TickEnd()) // ���������� ������ ����
		{
			if(m_is_melee) AttackDealDamage(attacked_unit);
			m_shape.setFillColor(CLR::White);
			m_is_atck_delay = false; // ��������� ����� �� �����
		}
	
		if(!attacked_unit->GetIsLife())
		{
			m_shape.setFillColor(CLR::White);
			m_is_atck_delay = false; // ��������� ����� �� �����
		}
	}
}

void UnitRPG :: SetSpeed(float speed) 
{
	m_ms_curr = speed;
}

void UnitRPG :: SetHp(uint hp) {
	m_hp_curr = hp;
	if(m_hp_deft < m_hp_curr) 
		m_hp_curr = m_hp_deft;
}

void UnitRPG :: SetMp(uint mp) {
	m_mp_curr = mp;
	if(m_mp_deft < m_mp_curr)
		m_mp_curr = m_mp_deft;
}

void UnitRPG :: SetHpMax(uint hp) 
{
	m_hp_bonus = hp - m_hp_deft;
	if(m_hp_bonus > 0) m_hp_curr = m_hp_bonus + m_hp_deft; 
	else m_hp_curr = m_hp_deft;
}

void UnitRPG :: SetMpMax(uint mp) 
{ 
	m_mp_bonus = mp - m_hp_deft;
	if(m_mp_bonus > 0) m_mp_curr = m_mp_bonus + m_mp_deft; 
	else m_mp_curr = m_mp_deft;
}

void UnitRPG :: SetState(const uint state) { m_state = (STATE)state; }

void UnitRPG :: Corpse() { if(timerP_Corpse.TickEnd()) m_shape.setFillColor(sf::Color(255,255,255, 255 / timerP_Corpse.order_current+1)); }

void UnitRPG :: AnimationPlay(string animID)
{
	if(animID == "Stand") 
	{
		m_shape.setTextureRect(sf::IntRect(24,0,24,32));
	}
	else
	{
		if(timerP_Animation.TickEnd()) 
		{
			if(animID == "MoveUp")		m_shape.setTextureRect(sf::IntRect(timerP_Animation.order_current * 24, 96, 24, 32));
			if(animID == "MoveDown")	m_shape.setTextureRect(sf::IntRect(timerP_Animation.order_current * 24, 0, 24, 32));
			if(animID == "MoveLeft")	m_shape.setTextureRect(sf::IntRect(timerP_Animation.order_current * 24, 32, 24, 32));
			if(animID == "MoveRight")	m_shape.setTextureRect(sf::IntRect(timerP_Animation.order_current * 24, 64, 24, 32));
		}
	}
}

bool UnitRPG :: GetIsAttackDelay() const			{ return m_is_atck_delay; }

int UnitRPG :: GetHpMax() const						{ return m_hp_deft + m_hp_bonus; }

int UnitRPG :: GetMpMax() const						{ return m_mp_deft + m_mp_bonus; }

int UnitRPG :: GetHp() const						{ return m_hp_curr; }

int UnitRPG :: GetMp() const						{ return m_mp_curr; }

bool UnitRPG :: GetIsLife() const					{ return m_is_life; }

float UnitRPG :: GetMoveSpeed() const				{ return m_ms_deft + m_ms_bonus; }

float UnitRPG :: GetAttackDist() const				{ return m_atck_dist_deft + m_atck_dist_bonus; }

int UnitRPG :: GetAttackDamage() const				{ return m_dmg_deft + m_dmg_bonus; }

enum UnitRPG :: STATE UnitRPG :: GetState() const	{ return m_state; }
