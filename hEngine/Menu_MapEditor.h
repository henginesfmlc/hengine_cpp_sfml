#pragma once
#include "BackGround.h"
#include "Level.h"
#include "MapEditor.h"
#include "LineForEnter.h"

class Menu_MapEditor : public Basis
{

public:

	vector<shared_ptr<Button>> vec_Button;
	vector<shared_ptr<LineForEnter>> vec_LineEnter;

	Menu_MapEditor(void);
	virtual void Update();
	virtual void Draw();
	virtual void Action();

	virtual ~Menu_MapEditor(void);
};

