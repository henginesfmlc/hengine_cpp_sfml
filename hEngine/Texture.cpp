#include "Texture.h"

Texture::Texture(void)
{
	// User Interface
	Load ( Cursor, "UI/Cursor.png" );
	Load ( Button, "UI/Button.png" );
	Load ( ButtonQuadIco, "UI/ButtonQuadIco.png");
	Load ( Glow, "SpecEffect/Glow.png" );

	// Game Objects
	Load ( DarkEye, "SpecEffect/DarkEye.png" );

	Load ( PortalEffectFire, "SpecEffect/PortalEffectFire.png" );
	Load ( PortalEffectFrost, "SpecEffect/PortalEffectFrost.png" );
	Load ( PortalEffectLife, "SpecEffect/PortalEffectLife.png" );
	Load ( PortalEffectShadow, "SpecEffect/PortalEffectShadow.png" );
	Load ( Lighting_16, "SpecEffect/Lighting_Anim_x400y204x16fraps.png" );
	Load ( Cubex, "UI/Cubex.png" );

	Load ( Wall_128x128_3x3_x4_01, "Doodas/Wall_128x128_3x3_x4_01.png" );
	Load ( WallSky, "Doodas/WallSky.png" );
	Load ( WallSkyClear, "Doodas/WallSkyClear.png" );
	Load ( WallSkyLine, "Doodas/WallSkyLine.png" );
	Load ( WallSkyHalfLine, "Doodas/WallSkyHalfLine.png" );

	// Unit
	Load ( Emitter, "Unit/Emitter.png" );

	for(int i = 0; i < 2; i++) Load ( Box[i], "Doodas/Box" + to_string(i) + ".png" );
	
}

void Texture::setSmoth(bool isSmooth)
{
	// User Interface
	Cursor.setSmooth(isSmooth);
	Button.setSmooth(isSmooth);
	ButtonQuadIco.setSmooth(isSmooth);

	// Game Objects
	PortalEffectFire.setSmooth(isSmooth);
	PortalEffectFrost.setSmooth(isSmooth);
	PortalEffectLife.setSmooth(isSmooth);
	PortalEffectShadow.setSmooth(isSmooth);
	Glow.setSmooth(isSmooth);
	Cubex.setSmooth(isSmooth);
	Lighting_16.setSmooth(isSmooth);
	for(int i = 0; i < 2; i++) Box[i].setSmooth(isSmooth);
	Wall_128x128_3x3_x4_01.setSmooth(isSmooth);
	//Unit
	Emitter.setSmooth(isSmooth);
}

inline void Texture::Load( IMG& texture, const string& file )
{
	texture.loadFromFile("Resources/Texture/" + file );
}

Texture::~Texture(void)
{
}
