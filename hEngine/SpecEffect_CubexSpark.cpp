#include "SpecEffect.h"

SpecEffect_CubexSpark::SpecEffect_CubexSpark(v2f pos, v2f siz, IMG& img, sf::Color color, float angle, float timer_life, float speed) 
	: SpecEffect(timer_life), angle(angle), speed(speed)
{
	//vector<shared_ptr<auto_ptr<b2BodyDef>>> vec_b2Body;
	//bd = new b2BodyDef;
	//bd->type = b2BodyType::b2_dynamicBody;
	//
	//b2body = world->CreateBody(bd);
	//b2PolygonShape* poly = new b2PolygonShape;
	//poly->SetAsBox(shape.getSize().x, shape.getSize().y, b2v(shape.getPosition().x, shape.getPosition().y), shape.getRotation());
	//b2Fixture* fixt = b2body->CreateFixture(poly, 0);
	//b2body->CreateFixture(poly,0);
	//b2body->SetTransform(b2v(shape.getPosition().x, shape.getPosition().y), shape.getRotation());
	//b2body->SetFixedRotation(true);
	//
	//for(int i=0; i < 10; ++i)
	//{
	//	b2Body boxGround = bd->CreateBox(BodyType.StaticBody, 0.5F, 0.5F, 2);
	//	boxGround.setTransform(i,0,0);
	//	boxGround.getFixtureList().get(0).setUserData("bd");
	//	boxGround = createBox(BodyType.StaticBody, 0.5F, 0.5F, 0);
	//	boxGround.setTransform(i,height-1,0);
	//	boxGround.getFixtureList().get(0).setUserData("b");
	//}

	ConstructShape(shape, pos, siz, img, false);
	shape.setFillColor(color);
	this->shape.setRotation(shape.getRotation());
}

SpecEffect_CubexSpark::SpecEffect_CubexSpark(v2f pos, v2f siz, sf::Color color, float angle, float timer_life, float speed) 
	: SpecEffect(timer_life), angle(angle), speed(speed)
{
	ConstructShape(shape, pos, siz, false);
	shape.setFillColor(color);
	this->shape.setRotation(shape.getRotation());
}

SpecEffect_CubexSpark::SpecEffect_CubexSpark(Shape& shape, float timer_life, float speed) 
	: SpecEffect(timer_life), shape(shape), angle(shape.getRotation()), speed(speed)
{
} 

void SpecEffect_CubexSpark::Update()
{
	timer_life -= time;
	if(timer_life < 0) timer_life = 0;
	shape.rotate(-.1 * time);
	const sf::Color& c = shape.getFillColor();
	const float factor = timer_life / TIMER_LIFE;
	shape.setFillColor(sf::Color(c.r, c.g, c.b, factor * 100));
	shape.setOutlineColor(sf::Color(c.r*.8, c.g*.8, c.b*.8, factor * 100));
	shape.setScale(v2f(factor,factor));
	MoveToAngle(shape, speed*factor, angle);
}

void SpecEffect_CubexSpark::Draw()
{
	wnd->draw(shape);
}

SpecEffect_CubexSpark::~SpecEffect_CubexSpark() { }