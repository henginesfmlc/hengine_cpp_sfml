#include "MainMenu.h"

MainMenu::MainMenu(void) : StateMainMenu(MSTATE::MAP_EDITOR)
{
	OpenMainMenu();
}

void MainMenu::Draw()
{
	BG.Draw();
	for(auto& button : vec_Button) button->Draw();
	switch (StateMainMenu)
	{
	case MainMenu::MAIN:
		break;
	case MainMenu::SETTINGS:
		break;
	case MainMenu::FAST_GAME:
		break;
	case MainMenu::LOAD_GAME:
		break;
	case MainMenu::COMPANY:
		break;
	case MainMenu::MAP_EDITOR:
		menu_MapEditor->Draw();
		break;
	case MainMenu::EXIT:
		break;
	default:
		break;
	}
}

void MainMenu::Action()
{
	BG.Action();
	switch (StateMainMenu)
	{
		case MainMenu::MAIN:

			for(auto& button : vec_Button) 
			{
				if(button->Action())
				{
					if(button->GetID() == "Company")	{ OpenCompany();	break; }
					if(button->GetID() == "Fast Game")	{ OpenFastGame();	break; }
					if(button->GetID() == "Load Game")	{ OpenFastGame();	break; }
					if(button->GetID() == "Settings")	{ OpenSettings();	break; }
					if(button->GetID() == "Map Editor")	{ OpenMapEditor();	break; }
					if(button->GetID() == "Exit")		{ OpenExit();		break; }
				}
			}

		break; // ����� ��������� MainMenu::MAIN:

//=================================================================================================================================
//=================================================================================================================================
//=================================================================================================================================	

		case MainMenu::SETTINGS:

			for(auto& button : vec_Button) 
			{
				if(button->Action())
				{
					if(button->GetID() == "Video")		{ OpenMainMenu();	break; }
					if(button->GetID() == "Back")		{ OpenMainMenu();	break; }
					if(button->GetID() == "Accept")		{ OpenMainMenu();	break; }
				}
			}

		break; // ����� ��������� MainMenu::SETTINGS:

//=================================================================================================================================
//=================================================================================================================================
//=================================================================================================================================

		case MainMenu::FAST_GAME:

			for(auto& button : vec_Button) 
			{
				if(button->Action())
				{
					if(button->GetID() == "Fast Game") { OpenMainMenu();  break;}
				}
			}

		break; // ����� ��������� MainMenu::FAST_GAME:

//=================================================================================================================================
//=================================================================================================================================
//=================================================================================================================================

		case MainMenu::LOAD_GAME:

			for(auto& button : vec_Button) 
			{
				if(button->Action())
				{
					if(button->GetID() == "Load Game") { OpenMainMenu();  break;}
				}
			}

		break; // ����� ��������� MainMenu::LOAD_GAME:

//=================================================================================================================================
//=================================================================================================================================
//=================================================================================================================================

		case MainMenu::COMPANY:

			for(auto& button : vec_Button) 
			{
				if(button->Action())
				{
					if(button->GetID() == "Company") { OpenMainMenu();  break;}
				}
			}

		break; // ����� ��������� MainMenu::COMPANY:

//=================================================================================================================================
//=================================================================================================================================
//=================================================================================================================================

		case MainMenu::MAP_EDITOR:

			menu_MapEditor->Action();

		break; // ����� ��������� MainMenu::MAP_EDITOR:

//=================================================================================================================================
//=================================================================================================================================
//=================================================================================================================================

		case MainMenu::EXIT:

			for(auto& button : vec_Button) 
			{
				if(button->Action()) 
				{
					if(button->GetID() == "Exit") { OpenMainMenu();  break;}
				}
			}

		break; // ����� ��������� MainMenu::EXIT
	}
}



void MainMenu::Update()
{
	BG.Update();
	switch (StateMainMenu)
	{
	case MainMenu::MAIN:
		break;
	case MainMenu::SETTINGS:
		break;
	case MainMenu::FAST_GAME:
		break;
	case MainMenu::LOAD_GAME:
		break;
	case MainMenu::COMPANY:
		break;
	case MainMenu::MAP_EDITOR:
		menu_MapEditor->Update();
		break;
	case MainMenu::EXIT:
		break;
	default:
		break;
	}
}

void MainMenu::OpenMainMenu()
{
	ClearButton();
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-44), v2f (16,4), texture->ButtonQuadIco, 2, "Company")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-40), v2f (16,4), texture->ButtonQuadIco, 2, "Fast Game")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-36), v2f (16,4), texture->ButtonQuadIco, 2, "Load Game")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-32), v2f (16,4), texture->ButtonQuadIco, 2, "Settings")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-28), v2f (16,4), texture->ButtonQuadIco, 2, "Map Editor")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-24), v2f (16,4), texture->ButtonQuadIco, 2, "Exit")));
	StateMainMenu = MSTATE::MAIN;
}

void MainMenu::OpenSettings()
{
	ClearButton();
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-44), v2f(16,4), texture->ButtonQuadIco, 2, "Video")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-40), v2f(16,4), texture->ButtonQuadIco, 2, "Audio")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-44,-28), v2f(8,4),  texture->ButtonQuadIco, 2, "Back")));
	vec_Button.push_back(make_shared<Button>(Button(v2f(-36,-28), v2f(8,4),  texture->ButtonQuadIco, 2, "Accept")));
	StateMainMenu = MSTATE::SETTINGS;
}

void MainMenu::OpenFastGame()
{
	ClearButton();
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-44), v2f(16,4), texture->ButtonQuadIco, 2, "Fast Game")));
	StateMainMenu = MSTATE::FAST_GAME;
}

void MainMenu::OpenCompany()
{
	ClearButton();
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-44), v2f(16,4), texture->ButtonQuadIco, 2, "Company")));
	StateMainMenu = MSTATE::COMPANY;
}

void MainMenu::OpenMapEditor()
{
	ClearButton();
	menu_MapEditor.reset(new Menu_MapEditor());
	StateMainMenu = MSTATE::MAP_EDITOR;
}

void MainMenu::OpenExit()
{
	ClearButton();
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-44), v2f(16,4), texture->ButtonQuadIco, 2, "Exit")));
	StateMainMenu = MSTATE::EXIT;
}

void MainMenu::OpenLoadGame()
{
	ClearButton();
	vec_Button.push_back(make_shared<Button>(Button(v2f(-40,-44), v2f(16,4), texture->ButtonQuadIco, 2, "Load Game")));
	StateMainMenu = MSTATE::LOAD_GAME;
}

void MainMenu::ClearButton()
{
	vec_Button.clear();
}

MainMenu::~MainMenu(void)
{
}
