#pragma once
#include "BackGround.h"

// �������� �� �������

class Grid : public B
{
private:

	bool isAvailable; // ������ ��������?

public:

	Shape shape;
	static CLR grid_clr;
	
	Grid(v2f pos, int style);
	virtual void NextStyle(); // ������ ��� ������
	virtual void Draw();
	virtual void Style(sf::Color color, float outline, sf::Color outcolor);
	// �������� ��� ��������� ��� �������� ������� �� ������
	virtual void SetAvailable(bool isAvail);
	// �������� �� ��������� ������
	virtual bool GetAvailable() const;
	virtual ~Grid();
};

class Sector : public B
{
public:

	vector<shared_ptr<Grid>> vec_Grid;
	static sf::FloatRect rect_sector;
	const v2i siz;

	Sector(v2i siz, v2f pos) : siz(siz)
	{
		for(int i = 0; i < siz.x; i++)
			for(int j = 0; j < siz.y; j++) 
				vec_Grid.push_back(make_shared<Grid>(Grid(v2f(pos.x+(i*size_grid.x), pos.y+(j*size_grid.y)), 1)));

		rect_sector = sf::FloatRect(pos.x, pos.y, pos.x + siz.x, pos.y + siz.y);
	}

	virtual void Draw()
	{
		if(GetIsActive())	// ���� ������ � ������
		{
			for(auto& grid : vec_Grid) 
				grid->Draw();
		}
	}

	virtual bool GetIsActive() // ��������� ����� �� 4 ������ ������� ������� �� ��������������� � �������� ������
	{ 
		return 
			vec_Grid.front()->shape.getGlobalBounds().intersects(GetVisible())
		|| vec_Grid.back()->shape.getGlobalBounds().intersects(GetVisible())
		|| vec_Grid[siz.y-1]->shape.getGlobalBounds().intersects(GetVisible())
		|| vec_Grid[(siz.y*siz.x)-siz.y]->shape.getGlobalBounds().intersects(GetVisible());
	}

	virtual ~Sector() {}
};