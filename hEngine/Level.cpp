#include "Level.h"

Level::Level(v2i size_map, string name) : TimerP_Regeneration(1,250)
{

	v2i num_Grid_In_Sector(scr_W/(size_grid.x*scr_1)/2, scr_H/(size_grid.y*scr_1)/2);
	v2i num_Sectors(size_map.x/num_Grid_In_Sector.x, size_map.y/num_Grid_In_Sector.y);

	for(int i = 0; i < num_Sectors.x; i++)
		for(int j = 0; j < num_Sectors.y; j++)
		{
			v2f pos = v2f(i*num_Grid_In_Sector.x*size_grid.x, j*num_Grid_In_Sector.y*size_grid.x);	// ����� ������ ��������� �������
			vec_Sector.push_back(make_shared<Sector>(Sector(num_Grid_In_Sector, pos)));				// ��������� ������ ��������
		}

	// ��������� ����� ����� � ������ ���� ������
	center_map = v2f(num_Sectors.x * num_Grid_In_Sector.x * size_grid.x * scr_1, num_Sectors.y * num_Grid_In_Sector.y * size_grid.y * scr_1) / 2.f;
	cam.setCenter(center_map);
	wnd->setView(cam);

	vec_B2Shape.push_back(make_shared<B2Shape>(B2Shape(center_map+v2f(0,scr_1*35), v2f(10,10))));

	for(int i = 0; i < 30; i++)
	{
		vec_UnitRPG.push_back(make_shared<UnitRPG>(UnitRPG(center_map+v2f(rand()%20-10, rand()%20-10), size_grid, 250, 100, 0.05f, 25, 15, false, "RangeUnit")));
		vec_UnitRPG.push_back(make_shared<UnitRPG>(UnitRPG(center_map+v2f(rand()%20-10, rand()%20-10), size_grid*1.5f, 250, 100, 0.05f, 25, 5, true, "MeleeUnit")));
	}

	for(int i = 0; i < 25; i++) 
		for(int j = 0; j < 20; j+=2)
			vec_Box.push_back(make_shared<Box>(Box(v2f((center_map.x / scr_1) + (i*size_grid.x), (center_map.y / scr_1) + j*4), size_grid, sf::Color::Yellow, scr_1/3.f, sf::Color::Green)));

	vec_Hero.push_back(make_shared<HeroCubex>(HeroCubex(center_map, size_grid, 0.03f, 100, "HeroCubex", 8)));
}

bool Level::LoadOnFile()
{
	return 0;
}

void Level::Restart()
{

}

void Level::Update()
{
	bool isHeroBound = false;

	for(auto& box : vec_Box)
	{
		for(auto& hero : vec_Hero)
		{
			for(int i = 0; i < hero->GetElements(); i++)
			{
				Shape& h = hero->m_vec_Part[i]->shape;
				if(GetBoundRect(h,box->shape)) 
				{
					isHeroBound = true;
					h.setFillColor(sf::Color(255,0,0));
				}
				else if(!isHeroBound) hero->Recolor();
			}
		}
	}


	for(auto& b2shape : vec_B2Shape)
	{
		b2shape->Update();
	}

	for(auto& hero : vec_Hero) hero->Update();

	if(TimerP_Regeneration.TickEnd()) 
	{
		for(auto& unit : vec_UnitRPG)
		{
			if(unit->GetIsLife())
			{
				unit->RestoreHp(1);
				unit->RestoreMP(1);
			}
		}
	}
	
	for(auto& it1 = vec_UnitRPG.begin(); it1 !=  vec_UnitRPG.end();)
	{
		auto& u1 = *(*it1);
		if(u1.GetIsLife()) 
		{
			u1.Update();
			for(auto& it3 = vec_Missle.begin(); it3 !=  vec_Missle.end();) 
			{
				auto& missle = *(*it3);
				missle.Update();

				if(GetBoundRect(missle.GetShape(), u1.m_shape)) 
				{
					u1.AttackDealDamage(missle.m_target_unit);
					it3 = vec_Missle.erase(it3);
				}
				else it3++;
			}

			for(auto& it2 = vec_UnitRPG.begin(); it2 != vec_UnitRPG.end();)
			{
				auto& u2 = *(*it2);
				if( ( it1 != it2 ) && u2.GetIsLife() && GetBoundRect(u1.m_shape, u2.m_shape) )
				{
					MoveToAngle(u1.m_shape, u1.GetMoveSpeed(), GetAngle(u2.m_shape, u1.m_shape));
				}
				it2++;
			}

			for(auto& box = vec_Box.begin(); box != vec_Box.end();)
			{
				auto& b = *(*box);
				if(GetBoundRect(u1.m_shape, b.shape)) 
					MoveToAngle(u1.m_shape, u1.GetMoveSpeed(), GetAngle(b.shape, u1.m_shape)); 
				box++;
			}
		}
		else u1.Corpse(); 
		it1++;
	}

	
	for(auto& sector : vec_Sector)
	{
		if(sector->GetIsActive()) // ����� ��������� ������� �� ���������� � ������
		{
			for(auto& grid : sector->vec_Grid)
			{
				if(Sector::rect_sector.intersects(shpCursor.getGlobalBounds()))
				{
					if(GetBoundRect(shpCursor, grid->shape))
					{
						grid->shape.setFillColor(sf::Color(200,200,100,200));
						grid->shape.setOutlineColor(sf::Color(100,100,50,150));
						grid->shape.setScale(v2f(0.9, 0.9));
					}
					else
					{
						grid->shape.setFillColor(grid->grid_clr);
						grid->shape.setScale(v2f(1, 1));
					}
				}
			}
		}
	}
}

void Level::Draw()
{
	for(auto& sector : vec_Sector)		sector->Draw();
	for(auto& box : vec_Box)			box->Draw();
	for(auto& unit : vec_UnitRPG)		unit->Draw();
	for(auto& hero : vec_Hero) 			hero->Draw(); 
	for(auto& missle : vec_Missle)		missle->Draw();
	for(auto& b2box : vec_B2Shape)		b2box->Draw();
}

void Level::Action()
{
	for(auto& hero : vec_Hero) hero->Action();
	if(event.type == sf::Event::MouseButtonPressed)
	{
		if(event.key.code == sf::Mouse::Left) vec_B2Shape.push_back(make_shared<B2Shape>(B2Shape(cur_p, v2f(4,4))));
		if(event.key.code == sf::Mouse::Right) vec_B2Shape.push_back(make_shared<B2Shape>(B2Shape(cur_p, v2f(20,2), true)));
	}
}

Level::~Level(void)
{
}
