#include "BodyPart.h"

BodyPart::BodyPart(v2f pos, v2f siz, const string name) : name(name)
{
	ConstructShape(shape, pos, siz, false);
}

void BodyPart::Update()
{
	
}

void BodyPart::Draw()
{
	wnd->draw(shape);
}

string& BodyPart::GetName() 
{ 
	return name; 
}

// void BodyPart::SetParam(){}

BodyPart::~BodyPart() 
{

}