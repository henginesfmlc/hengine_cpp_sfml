#pragma once
#include "BodyPart.h"
#include "SpecEffect.h"
#include "UnitRPG.h"

class Missle : public Basis
{
protected:

	vector<shared_ptr<SpecEffect>> vec_SpecEffect;
	vector<shared_ptr<BodyPart>> vec_Body;
	TimerPereodic timerP_CreateSpecEffect;

	float speed;
	const float damage;

public:

	Missle(v2f pos, v2f siz, float speed, int dmg, float angle, sf::Color color);
	virtual float GetDamage() const;
	virtual void Update();
	virtual void Draw();
	virtual void SpecEffectUpdate();
	virtual Shape& GetShape();
	virtual ~Missle(void);
};

class MissleTarget : public Missle
{
public:

	UnitRPG *m_target_unit;
	MissleTarget(v2f pos, v2f siz, float speed, int dmg, float angle, UnitRPG& target_unit, IMG& img);
	virtual void Update();
	virtual ~MissleTarget(void);
};
