#pragma once
#include "RightClickMenu.h"
#include "Hero.h"
#include "UnitRPG.h"
#include "Grid.h"

class MapEditor : public B
{
private:

	BackGround BG;
	Shape brush_shape_ghost_object;
	string map_name;
	auto_ptr<Hero> hero;
	vector<shared_ptr<SpecEffect>> vec_SpecEffect;
	vector<shared_ptr<RightClickMenu>> vec_RCMenu;
	vector<shared_ptr<Sector>> vec_Sector;
	vector<shared_ptr<UnitRPG>> vec_Unit;

	auto_ptr<Unit> buffer_Unit;
	enum STATE { ObjectInstall, InRightCM, InButtonCursor, Default } state; 

public:

	virtual void CheckSizeBrush(string matrix__2x2__5x5_);
	virtual B SelectObject();
	virtual void SaveMap(string file_name);
	virtual void Draw();
	virtual void Action();
	virtual void Update();
	virtual void NewMap(v2f size, v2f greed_size, string name);
	virtual bool LoadMap(string file_name);
	virtual string PickedObject();

	MapEditor(v2i size_map = v2i(60,100), string name = "EmptyMap");
	~MapEditor(void);
};

