#include "Game.h"

Game::Game(void)
{
	B::size_grid = v2f(.5f,.5f);

	vec_MainMenu.push_back(make_shared<MainMenu>(MainMenu()));
	vec_MapEditor.push_back(make_shared<MapEditor>(MapEditor(v2i(200,200), "Map1")));
	vec_Level.push_back(make_shared<Level>(Level(v2i(200,200), "NewMap01")));

	thread_Update = new sf::Thread(&Game::ThreadUpdate, this);
	thread_Update->launch();

	while (B::wnd->isOpen())
	{
		while (B::wnd->pollEvent(B::event))
		{
			
			switch (B::GameState)
			{
				case B::MENU:		for(auto& menu : vec_MainMenu) menu->Action();		break;
				case B::GAME:		for(auto& level : vec_Level) level->Action();		break;
				case B::MAP_EDITOR: for(auto& editor : vec_MapEditor) editor->Action();	break;
					default: break;
			}
			if(B::event.type == sf::Event::Closed) B::wnd->close();
			if(B::event.type == sf::Event::KeyPressed) 
			{
				if(B::event.key.code == Key::Escape) B::wnd->close();
				if(B::event.key.code == Key::Num1) B::GameState = B::MENU;
				if(B::event.key.code == Key::Num2) B::GameState = B::MAP_EDITOR;
				if(B::event.key.code == Key::Num3) B::GameState = B::GAME;
			}
		}
		sf::sleep(sf::milliseconds(25));
	}

	vec_Level.clear();
	vec_MainMenu.clear();
	vec_MapEditor.clear();
	thread_Update->terminate();
}

void Game::ThreadUpdate()
{
	while (B::wnd->isOpen())
	{
		B::wnd->setVerticalSyncEnabled(false);
		B::Update();
		switch (B::GameState)
		{
			case B::MENU:		for(auto& menu : vec_MainMenu) menu->Update();		break;
			case B::GAME:		for(auto& level : vec_Level) level->Update();		break;
			case B::MAP_EDITOR: for(auto& editor : vec_MapEditor) editor->Update();	break;
				default: break;
		}

		B::wnd->clear();

		switch (B::GameState)
		{
			case B::MENU:

				for(auto& menu : vec_MainMenu) 
				{
					const float& a = B::cam.getRotation();
					const v2f& p = B::cam.getCenter();
					B::cam.setCenter(0,0);
					B::cam.setRotation(0);
					B::wnd->setView(B::cam);
					B::cur_p = B::wnd->mapPixelToCoords(sf::Mouse::getPosition(*B::wnd));

					// ��� ������ ������ ��������� �������� ���������� � ��������� � ������

					menu->Draw();

					B::cam.setCenter(p);
					B::cam.setRotation(a);
					B::wnd->setView(B::cam);
					B::cur_p = B::wnd->mapPixelToCoords(sf::Mouse::getPosition(*B::wnd));
				}

				break;
			case B::GAME:		for(auto& level : vec_Level) level->Draw();			break;
			case B::MAP_EDITOR: for(auto& editor : vec_MapEditor) editor->Draw();	break;
		}

		B::wnd->draw(B::shpCursor);

		// ������� ���
		static sf::Text text_fps;
		B::ConstructText(text_fps, B::cam_p-v2f(B::cam.getSize()/2.15f), 2,"FPS: " + to_string(int(1000/B::time)), B::font_freshman, sf::Color::Cyan);
		B::wnd->draw(text_fps);
		// ����� ��������

		B::wnd->display();
	}
}

Game::~Game(void)
{

}
